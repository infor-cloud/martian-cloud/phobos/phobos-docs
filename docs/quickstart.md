---
title: Quickstart
description: "Learn the basics of Phobos."
---

Let's get started! This guide will walk you through the basics of Phobos, from defining a simple pipeline template and executing it to creating a release.

## Prerequisites

Before you begin, ensure you have the following:

- Phobos CLI
- Phobos UI

### Phobos CLI

- Ensure you have the latest version of the Phobos CLI installed on your machine. If you don't have it, download it from the [releases page](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/-/releases).
- After installing the CLI, run `phobos configure` to set up your endpoint and profile.

#### Logging into the Phobos CLI

```shell title="Login to the default Phobos instance"
phobos sso login
```

### Phobos UI

- Ensure you have the latest version of the Phobos UI. If you don't have it, you can access the repository from [GitLab](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-ui).

:::tip important
In both the CLI and the UI, Phobos should be configured to the **api** endpoint, `https://api.phobos.example.com`, **not** the UI endpoint. If you have issues during the CLI login process or running the Phobos UI, confirm that you are using the api endpoint.

After successfully logging in via the CLI, you can update your profiles and endpoints by running `phobos configure`. Run `phobos configure -h` for more information.
:::

:::tip
More information on setting up the Phobos CLI and the Phobos UI can be found in the [CLI](/docs/setup/cli.md) and [UI](/docs/setup/ui.md) guides.
:::

## Create a project

```shell title="Create a project underneath an organization"
phobos project create -org-name "neutron" my-project
```

> **Note:** Replace `neutron` with your organization name. If you don't have an organization, ask a system administrator to create one for you.

## Define a pipeline template

Create a new file called `pipeline.hcl` with the following content:

<details>
<summary>Click to view the pipeline template</summary>

```hcl title="pipeline.hcl"
plugin exec {}

stage "test" {
  task "preliminary" {
    action "exec_command" {
      command = <<EOF
          echo "This action will run before the main task."
        EOF
    }
  }
}

stage "deploy" {
  task "main" {
    action "exec_command" {
      command = <<EOF
        echo "This is a command that will sleep for 10 seconds"
        sleep 10
      EOF
    }
  }
}

stage "cleanup" {
  task "final" {
    action "exec_command" {
      command = <<EOF
          echo "This action will only run if the previous action was successful."
          echo "The exit code of the deploy's main task was ${action_outputs.stage.deploy.task.main.action.exec_command.exit_code}."
        EOF
    }
  }
}
```

Let's break down the pipeline template:

- The `plugin` block defines the plugin that will execute the pipeline. In this case, we're using the `exec` plugin, which simply executes a script. It's a built-in plugin that comes with Phobos. Each `plugin` must be defined in a separate block.

- The `stage` block defines a stage in the pipeline. It contains one or more `task` blocks. Each `task` block contains one or more `action` blocks. A stage is a logical grouping of tasks. In this example, we have three stages: `test`, `deploy`, and `cleanup`.

- The `task` block defines a task in the pipeline. A task is a logical grouping of actions. In this example, we have one task in each stage: `preliminary`, `main`, and `final`. A task can have zero or more actions.

- The `action` block defines an action in the pipeline. An action is a single unit of work. In this example, we have one action in each task: `exec_command`. An action can have zero or more outputs.

- The `exec_command` action is used to execute a shell command. It takes a single argument, `command`, which is the command to execute. The output of this action is the exit code of the command.

- The `<<EOF` syntax is called a heredoc. It's a way to define multi-line strings in HCL. The `EOF` at the end of the block is called the delimiter. It's used to mark the end of the heredoc. This is a common way to define multi-line shell commands.

- The `${action_outputs.stage.deploy.task.main.action.exec_command.exit_code}` syntax is used to reference the output of the `exec_command` action in the `deploy` stage's `main` task. This is how you can pass data between actions in a pipeline.

</details>

## Validate the pipeline template

**Always** validate your pipeline template before using it. This will ensure that the template doesn't contain any syntax errors among other things.

```shell title="Validate the pipeline template"
phobos pipeline validate
```

> **Note:** If you're not in the same directory as the `pipeline.hcl` file, you can specify the path to the file using the `-directory` flag and optionally the `-filename` flag to point to the file. You will have to be authenticated to a Phobos instance to run this command.

## Create a pipeline from the template

Now that you have a valid pipeline template, you can create a pipeline from it.

```shell title="Create a pipeline from the template"
phobos pipeline create -org-name "neutron" -type "runbook" -follow my-project
```

The pipeline details can be viewed in the Phobos UI within the **Pipelines** section of the project.

:::important
Replace `neutron` with your organization name and `my-project` with your project name. We are using the `-follow` flag to follow the pipeline's progress. If you don't want to follow the pipeline's progress, you can omit this flag.

The `-type` flag specifies the type of pipeline and is required. In this case, we're creating a `runbook` pipeline. Learn more about [Pipelines](/docs/guides/pipelines.md).
:::

## Creating a release

Now let's create a release!

### Create a versioned pipeline template

To create a release, we need a versioned copy of the pipeline template we just created.

```shell title="Create a versioned pipeline template"
phobos pipeline-template create -org-name neutron -project-name my_project -versioned -semantic-version 0.1.0 -name deploy
```

> **Note:** You can also create a versioned pipeline template via the Phobos UI. Navigate to the **Pipeline Templates** section of the project and select **New Pipeline Template**. Fill in the required fields and click **Create Pipeline Template**.

### Create a release lifecycle

Let's move to the UI to continue creating the release.

In the Phobos UI, navigate to your organization `neutron` and find your project `my_project`. Select **Release Lifecycles** from the project sidebar. Then select **New Release Lifecycle** to navigate to the new release lifecycle form.

On the **New Release Lifecycle** form, enter a name, `my_release_lifecycle`, for the release lifecycle. Select **Enter HCL Data** to use the sample release lifecycle provided by the editor, also shown here:

```hcl
stage "dev" {
  deployment {
    environment = "dev"
  }
}

stage "prod" {
  deployment {
    environment = "prod"
  }
}
```

Then click **Create Release Lifecycle**.

![New Release Lifecycle](../static/img/quickstart/quickstart-new-release-lifecycle.png "New Release Lifecycle")

### Create a new release

Now we have the minimum requirements to create a release:

- A versioned pipeline template
- A release lifecycle

Navigate to the **Releases** section of the project and select **New Release** to go to the new release form.

On the **New Release** form, enter `0.1.0` as the semantic version.

![New Release - semantic version](../static/img/quickstart/quickstart-new-release-semantic-version.png "New Release - semantic version")

Scroll down the form page to the **Release Lifecycle** dropdown and select `my_release_lifecycle`.

After selecting `my_release_lifecycle`, two deployments will generated in the **Deployments** section. For each deployment, select the pipeline template we created earlier, `deploy`. By default, the latest version will populate in the **Version** dropdown.

Click **Create Release**.

![Create Release](../static/img/quickstart/quickstart-create-release.png "Create Release")

You'll be navigated to the release details page.

![Release Details Page](../static/img/quickstart/quickstart-release-details-page.png "Release Details Page")

<span style={{ fontSize: '1.5em' }}>🔥🔥 Congratulations! You've just learned the basics of Phobos! 🔥🔥</span>
