---
title: Built-in
description: "A list of built-in plugins available in Phobos."
---

## Built-in plugins

These are the current built-in plugins that come with Phobos out of the box:

- [Exec](exec.md)
