---
title: Introduction
description: "An introduction to Phobos plugins."
---

Phobos plugins are the building blocks of your Phobos configuration. They allow you to perform a wide variety of tasks, such as executing shell commands, interacting with cloud providers, and more. Here we'll cover the basics of Phobos plugins, and how to use them in your Phobos configuration.

## What is a plugin?

A Phobos plugin is a reusable piece of code that can be used to perform a specific task. Each plugin has a set of inputs and outputs and can be used in a variety of ways to accomplish different tasks. For example, the `exec` plugin allows you to execute shell commands.

:::important For Tharsis users
The `tharsis` plugin is not built-in but can be easily accessed via the [Phobos plugin registry](https://gitlab.com/infor-cloud/martian-cloud/phobos/plugins). The Phobos plugin registry is a collection of plugins that have been created by the community and can be used in your Phobos configuration.
:::

## How to use a plugin?

To use a plugin in your Phobos configuration, you simply need to reference it in your configuration file. For example, to use the `exec` and `gitlab` plugins, you would add the following to your configuration:

```hcl showLineNumbers
plugin_requirements {
  gitlab = {
    source = "<registry-hostname>/<organization>/gitlab" # Hostname is optional when using the Phobos plugin registry
    version = ">= 0.1.0" # Optional
  }
}

plugin exec {}

plugin gitlab {
  api_url = "https://gitlab.com/api/v4"
  token   = "my-gitlab-token"
}

stage dev {
  task "start_job" {
    action "gitlab_run_job" {
      # Action configuration...
    }
    action "exec_command" {
      command = <<EOF
        echo "Hello, world!"
      EOF
    }
  }
}
```

<details>
<summary>Expand for an explanation</summary>

The `plugin_requirements` block is used to define the requirements for a plugin i.e. the `source` tells Phobos where to find the plugin and `version` (optional) specifies the version constraints for the plugin. The `registry-hostname` is optional when using the Phobos plugin registry. When using built-in plugins like `exec`, the `plugin_requirements` block is not required.

The `plugin` block is used to define the configuration for a plugin i.e. the `api_url` and `token` for the `gitlab` plugin. Different plugins have different configuration options, so be sure to check the documentation for the plugin you are using.

</details>

## Built-in plugins

Check out the current list of [built-in plugins](/docs/plugins/built-in.md) that come with Phobos out of the box. You'll also find documentation on how to use each plugin, and examples of how to use them in your Phobos configuration.

## External plugins

External plugins are plugins that are not built-in to Phobos, but can be accessed via the [Phobos plugin registry](https://gitlab.com/infor-cloud/martian-cloud/phobos/plugins). External plugins can be used in the same way as built-in plugins, by adding them to the `plugin_requirements` block in your template.

Currently, the following external plugins are available:

- [tharsis](https://gitlab.com/infor-cloud/martian-cloud/phobos/plugins/phobos-plugin-tharsis)
- [gitlab](https://gitlab.com/infor-cloud/martian-cloud/phobos/plugins/phobos-plugin-gitlab)

## Where can I find more plugins?

Keep an eye on the [Phobos plugin registry](https://gitlab.com/infor-cloud/martian-cloud/phobos/plugins) to find available plugins. Additionally, check out the [Phobos Plugin SDK](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk) to learn how to create your own custom plugins.

:::tip Reminder
Built-in plugins, like `exec`, do not require the `plugin_requirements` block in the configuration file. The `plugin_requirements` block is only required for custom plugins or plugins from the Phobos plugin registry.
:::

## Next steps

Now that you have an understanding of what Phobos plugins are and how to use them, you can explore the built-in plugins that come with Phobos, or create your own custom plugins. You can also check out the Phobos plugin registry to see what plugins are available to use in your Phobos configuration.
