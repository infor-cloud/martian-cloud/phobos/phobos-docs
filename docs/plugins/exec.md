---
title: Exec
description: "An explanation and usage of the exec plugin."
---

The built-in plugin called `exec` allows you to execute shell commands from within your HCL configuration.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Configuration

None

## Actions

| Name           | Description             |
| -------------- | ----------------------- |
| `exec_command` | Execute a shell command |

## exec_command

The `exec_command` action allows you to execute a shell command.

### Arguments

| Argument           | Description              | Type   | Required |
| ------------------ | ------------------------ | ------ | -------- |
| `command`          | The command to execute   | string | Yes      |
| `dot_env_filename` | Name of file for outputs | string | No       |

### Outputs

| Output      | Description                                | Type   |
| ----------- | ------------------------------------------ | ------ |
| `exit_code` | The exit code of the command               | number |
| `dot_env`   | Outputs parsed from dot_env_filename above | map    |

### Usage

```hcl showLineNumbers
plugin exec {}

stage "dev" {
  task "hello_world" {
    action "exec_command" {
      alias = "hello_exec"
      dot_env_filename = ".env"
      command = <<EOF
        echo "Hello, world!"
        echo "example_key=example_value" >> .env
      EOF
    }
  }
}

stage "stage-2" {
  task "get_outputs" {
    action "exec_command" {
      command = <<EOF
        echo "output value : ${action_outputs.stage.dev.task.hello_world.action.hello_exec.dot_env.example_key}"
      EOF
    }
  }
}
```

In this example, the `exec` plugin is used to execute the `echo "Hello, world!"` command. The output of this command will be

- the exit code of the command, which will be `0` if the command was successful.
- a map of any key-value pairs (strings) written to file `.env` in the first stage.

## Frequently Asked Questions (FAQ)

### How would I reference the output of this `exec` action in another?

The full path to the `exec` action can be used to get its output. For example, if you wanted to reference the `exit_code` output from the `exec` action in the example above, you would use `action_outputs.stage.dev.task.hello_world.action.exec_command.exit_code` in another action.

### How can I use a complex variable or output inside of an `exec` command?

Since the `command` argument only accepts a `string` type, you will need to convert the complex variable or output to a string before it can be used. The variable can be passed to the built-in `jsonencode` function to convert it to a string. For example, if you wanted to use the variable `var.my_variable` in an `exec` command, you would use `jsonencode(var.my_variable)` in the `command` argument.
