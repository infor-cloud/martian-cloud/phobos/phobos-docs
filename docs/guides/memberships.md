---
title: Memberships
description: "A guide to using memberships in Phobos."
---

## What are memberships?

Memberships are used to manage access to organizations and projects in Phobos. Each membership is associated with a role that defines what the user can do within the organization or project.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Types of memberships

There are three types:

- **User Membership**: A user membership is a direct association between a user and an organization or project. It is created when a user is added to an organization or project. It represents a human user.

- **Team Membership**: A team membership is an association between a team and an organization or project. It is created when a team is added to an organization or project. It represents a collection of human users.

- **Service Account Membership**: A service account membership is an association between a service account and an organization or project. It is created when a service account is added to an organization or project. It represents a machine user.

## Roles and permissions

Roles define what a [subject](#what-is-a-subject) can do within an organization or project. For instance, a `deployer` role can deploy a pipeline, while a `viewer` role can only view the pipeline.

The following roles are available:

- **Owner**: Can manage the organization or project, its members, and arbitrarily perform any action. This role should be used sparingly.
- **Deployer**: Can create or modify resources and perform most day-to-day operations. This role is sufficient for most use cases.
- **Viewer**: Can view the organization or project and its resources, but cannot make any changes.

## Creating a membership

To create a membership, navigate to the organization or project's page and click on the "Members" tab. Then click on the "Add Member" button.

Select either "User", "Team", or "Service Account" from the options and search for the user, team, or service account you want to add. Then select the role you want to assign to the user, team, or service account and click `Add Member`.

## Updating a membership

To update a membership, navigate to the organization or project's page, select the membership you want to update, and click on the "Pencil" icon. You can update the role and click `Save` to save the changes.

## Deleting a membership

To delete a membership, navigate to the organization or project's page, select the membership you want to delete, and click on the "X" icon.

:::warning
Deleting a membership will prevent the user, team, or service account from accessing the organization or project.
:::

## Frequently Asked Questions (FAQ)

### Who can create a membership?

Owner or higher roles can create a membership.

### What is a subject?

A subject represents an entity that performs an action on a resource. Generally, a subject can be a user, team, or service account.

### Why am I seeing a "[subject](#what-is-a-subject) is not authorized to perform the requested operation" error?

Generally, this error occurs when the subject is able to view the resource but not modify it. Please reach out to the organization or project owner to get the necessary role assigned to the subject.

### Why am I seeing a "Resource not found" error?

This error occurs when the [subject](#what-is-a-subject) is not a member of the organization or project. Please make sure that the [subject](#what-is-a-subject) is a member of the organization or project and has the necessary role assigned to it.

### Can a user from one organization access resources in another organization?

No, a user can only access resources within the organization it is a member of.

### I don't want to give my user access to all projects in my organization. What should I do?

You can assign a role to the user at the project level. Navigate to the project's page and click on the "Members" tab. Then click on the "Add Member" button and assign a role to the user.

### What's the benefit of a team membership over a user membership?

A team membership allows you to manage access to an organization or project for a group of users. This is useful when you want to give the same level of access to multiple users.

### Is it possible to have custom roles?

Yes. Although, only system administrators can create custom roles. Please reach out to your system administrator if you need a custom role.
