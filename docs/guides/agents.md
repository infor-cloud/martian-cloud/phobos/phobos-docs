---
title: Agents
description: "A guide to using agents in Phobos."
---

## What are agents?

Agents are used to run jobs in Phobos. They are responsible for executing the steps in a pipeline and reporting the results back to the API. The API already provides some shared agents, but you can also create your own.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Types of agents

There are two types:

- **Shared Agent**: A shared agent is a pre-configured agent that is shared across all organizations and projects. It comes by default with Phobos and is available to all organizations and projects. It's used when no organization agent is available.

- **Organization Agent**: An organization agent is a user-defined agent that is specific to an organization. It is created when a user adds an agent to an organization. It uses an organization service account to authenticate with the API.

## Creating an agent

To create an agent, navigate to the organization's page and click on the "Agents" tab. Then click on the "New Agent" button.

Fill in the agent's name, description, and enter tags if necessary. Optionally, uncheck the "Run Untagged Jobs" checkbox if you want to restrict the agent to only run jobs with matching tags. It's also possible to disable the agent by unchecking the "Enabled" toggle. Click `Create Agent` to create the agent.

Now, you must assign a service account to the agent. See [Assigning service account to an agent](#assigning-service-account-to-an-agent) for more information.

## Updating an agent

To update an agent, navigate to the agent's page, select the agent you want to update, and click on the "Edit" button.

You can update the description, tags, run untagged jobs, and enable/disable the agent. Click `Update Agent` to save the changes.

## Deleting an agent

To delete an agent, navigate to the agent's page, select the agent you want to delete, and click on the upside-down caret next to the "Edit" button. Then click on the "Delete Agent" button.

## Assigning service account to an agent

After creating an agent, you can assign a service account to it. Navigate to the agent's page and click on the "Assigned Service Account" tab. Then click on the "Assign Service Account" button. Search for the service account you want to assign to the agent and click `Assign Service Account`.

:::warning
Without a service account, the agent will not be able to authenticate with the API and consequently claim jobs.
:::

## Frequently Asked Questions (FAQ)

### Who can create an agent?

Owner or higher roles can create an agent.

### What is the difference between a shared agent and an organization agent?

A shared agent is a pre-configured agent that is shared across all organizations and projects. It comes by default with Phobos and is available to all organizations and projects. An organization agent is a user-defined agent that is specific to an organization.

### What's the advantage of using an organization agent?

An organization agent is specific to an organization and can be used to run jobs in a specific environment. It offers more control and flexibility over the shared agent.

### How are the tags used in an agent?

Tags are used to restrict the agent to only run jobs with matching tags. If the "Run Untagged Jobs" checkbox is unchecked, the agent will only run jobs with matching tags. The tags are defined within your HCL configuration.

### Can I use a service account from another organization with my agent?

No, a service account can only be assigned to an agent within the organization it is a member of.

### Can I view active agent sessions?

Yes, you can view active agent sessions by navigating to the agent's page and clicking on the "Sessions" tab.

### Can I view the job history of an agent?

Yes, you can view the job history of an agent by navigating to the agent's page and clicking on the "Jobs" tab.

### My agent is getting a "[subject] is not authorized to perform requested action" error. What should I do?

Please make sure the agent has a service account assigned to it.

### Which agent takes precedence when running a job?

The first agent that matches the job's tags will take precedence.
