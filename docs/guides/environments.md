---
title: Environments
description: "What are environments in Phobos and how do you use them?"
---

## What are environments?

Environments represent the location where an application or product can be deployed. For example, a specific AWS account would be represented using an environment. An environment is a logical concept, so multiple environments in Phobos can point to the same physical environment.

Environments will be created automatically if they do not already exist when a deployment is started. Environments can also be created manually.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Viewing environments

To view the environments, select **Environments** from the project sidebar. The environments page displays a list of all environments in the project. Select an environment to view its details page.

![Environments list](/img/environments/environments-list.png)

## Environment details page

The environment details page displays a list of deployments that have been created within the environment.

## Creating an environment

1. Go to the project sidebar and select **Environments**. Then click **New Environment**.
2. Enter the name of the environment.
3. Enter the description of the environment (optional).
4. Click **Create Environment**.

## Editing an environment

1. Go to the project sidebar and select **Environments**. Then click the environment you want to edit.
2. Select **Edit**.
3. Update the environment name or description.
4. Click **Update Environment**.

## Deleting an environment

1. Go to the project sidebar and select **Environments**. Then click the environment you want to delete.
2. Select the upside-down caret next to the **Edit** button.
3. Click **Delete Environment**.
4. A confirmation dialog will appear. Click **Delete** to confirm. Click **Cancel** to cancel the deletion.

:::important
Deleting an environment will not delete any deployments associated with the environment. The deployments will still exist in Phobos.
:::

## Environment protection rules

Environment protection rules designate which principals (users, teams, or service accounts) or roles (owner, release manager, developer, viewer) are able to deploy to specified environments. This feature is useful for ensuring that only authorized principals or members with specified roles can deploy to certain environments.

## Organization- and project-level environment protection rules

Environment protection rules can be created at the organization level or at the project level. If an environment protection rule is created at the organization level, it will apply to all of the environments in the organization that match the environment name. If an environment protection rule is created at the project level, it will only apply to environments that match the environment name in that project.

:::important
Environment protection rules are additive. If an environment protection rule, say `prod`, is created at the organization level and another environment protection rule, also named `prod`, is created at the project level, the rules will be combined. This means that both rules will need to be satisfied before a deployment to the `prod` environment can be made.
:::

## Viewing environment protection rules

1. Select **Settings** from either the organization or project sidebar.
2. At **Environments** click **Show** to view any existing environment protection rules.

![Environment protection rules - org list](/img/environments/environment_protection_rules/env-protect-rules-org-list.png "Environment protection rules - org list")

In the example above, this organization has two environment protection rules: `dev` and `prod`.

- **`dev`**: only the two principals listed under **Principals** are allowed to deploy to the `dev` environment. Or, only members with the role listed under **Roles** are allowed to deploy to the `dev` environment.

  - **Note:** the two principals listed under **Principals** do not need to have the role listed under **Roles** to deploy to the `dev` environment. Also, any member with the role listed under **Roles** can deploy to the `dev` environment, even if they are not listed under **Principals**.

- **`prod`**: only the two principals listed under **Principals** are allowed to deploy to the `prod` environment. No roles are specified for the `prod` environment, so there are no role restrictions to deploy to the `prod` environment.

:::info Environment protection rules names
Environment protection rules are named after the environment they protect. In the example above, the environment protection rule `dev` protects the `dev` environment.
:::

## Create an environment protection rule

1.  Go to the organization or project sidebar and select **Settings**.
2.  Click **Show** to the right of **Environments**.
3.  Click **Add Rule**.
4.  On the **New Environment Rule** page, enter the following information:

- **Environment**: the environment input is a dropdown and will list all available environments within projects of the organization. You can select one of the environments or you can enter a new environment name if the environment does not exist.

  :::info New environment name
  If you enter a new environment name, the rule will apply to the environment when it is created.
  :::

- **Principals**: Select principals (users, teams, service accounts) that you want to allow to deploy to the environment. You can select one or more principals.
- **Roles**: Select the roles that you want to allow to deploy to the environment. You can select one or more roles.

5.  Click **Create Environment Rule**.

## Edit an environment protection rule

1. Go to the organization or project sidebar and select **Settings**.
2. Click **Show** to the right of **Environments**.
3. Click on the pencil icon of the environment protection rule you want to edit.
4. Update the list of principals and/or roles.
5. Click **Update Environment Rule** to save your changes. Click on **Cancel** to discard your changes and return to the list of environment protection rules.

## Delete an environment protection rule

1. Go to the organization or project sidebar and select **Settings**.
2. Click **Show** to the right of **Environments**.
3. Click on the **X** icon of the environment protection rule you want to delete.
4. A confirmation dialog will appear. Click **Delete** to confirm. Click **Cancel** to cancel the deletion.

## Frequently Asked Questions (FAQ)

### What's the difference between an environment protection rule and an approval rule?

An environment protection rule restricts who can deploy to an environment. It can be thought of as a blanket rule that prevents unauthorized principals or members with certain roles from deploying to environments.

Approval rules are specifically meant to manage pipeline tasks and nested pipelines. They require manual review and approval from a specified approver before the pipeline task or nested pipeline can be executed. Approval rules generate a to-do item; users will receive a notification that they must approve the to-do item. Learn more about [Approval Rules](/guides/approval_rules).

### Can an environment protection rule be created for multiple environments? Or can an environment have more than one environment protection rule?

An environment protection rule can only be created for one environment at a time.

An environment can have only one environment protection rule. However, if an environment protection rule is created at the organization level, say `prod`, and another environment protection rule is created at the project level, also called `prod`, both rules will apply to the `prod` environment. This means that both rules will need to be satisfied before a deployment to the `prod` environment can be made.

### Who can update environment protection rules?

Only members with the `owner` or `release_manager` roles can update environment protection rules.

### Can I create an environment protection rule for an environment that doesn't exist yet?

Yes, you can create an environment protection rule for an environment that doesn't exist yet. When you create an environment protection rule for a non-existent environment, the rule will apply to the environment when it is created.

### I can't deploy to an environment although I'm listed as a principal in the organization/project environment protection rule. What should I do?

Double-check the environment protection rules in the project and in the parent organization. Both organization- and project-level environment protection rules must be satisfied.

Also, confirm that you are a member of the project and confirm your role (check **Members**) as that could affect your ability to deploy to an environment. For example, if a project-level environment protection rule designates that only members with the role of `developer` can deploy to an environment, you must have the `developer` role in the project to deploy to that environment.
