---
title: Service Accounts
description: "A guide to using service accounts in Phobos."
---

## What are service accounts?

Service accounts are used for Machine to Machine (M2M) authentication and use [OIDC](https://openid.net/connect/) federation to log in to Phobos. For instance, they allow a [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline to authenticate and interface with the API.

Service accounts can be scoped to a specific organization or project and can be assigned roles to access resources within the organization or project. Organization-scoped service accounts are accessible by all of the organization's projects. Project-scoped service accounts are only accessible within the project.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Creating a service account

To create a service account, navigate to the target organization or target project and click on "Service Accounts" from the sidebar. Then click on the "Create Service Account" button.

Fill in the name, an optional description, Identity Provider information with bound claims, and click `Create Service Account`.

Now, you must assign a role to the service account. See [Assigning roles to a service account](#assigning-roles-to-a-service-account) for more information.

## Updating a service account

To update a service account, navigate to the "Service Accounts" page, select the service account you want to update, and click on the "Edit" button.

You can update the description and Identity Provider information with bound claims. Click `Update Service Account` to save the changes.

## Deleting a service account

To delete a service account, navigate to the "Service Accounts" page, select the service account you want to delete, and click on the upside-down caret next to the "Edit" button. Then click on the "Delete Service Account" button.

:::danger
Deleting a service account will break any integrations that rely on it, such as CI/CD pipelines.
:::

## Assigning roles to a service account

After creating a service account, you can assign roles to it. Navigate to the organization or project's page and click on the "Members" tab. Then click on the "Add Member" button.

Select "Service Account" from the options and search for the service account you want to assign a role to. Then select the role you want to assign to the service account and click `Add Member`. Generally, a `developer` role is sufficient for most use cases.

:::warning
Without a role, the service account will not be able to access any resources in the organization or project.
:::

## Frequently Asked Questions (FAQ)

### Who can create a service account?

Deployer or higher roles can create a service account.

### How do I use a service account with the Phobos CLI?

See the [CLI documentation](/docs/cli/intro.md#service-account) for more information.

### Why is my service account not working?

Please make sure that the service account is a member of the organization or the project and has the necessary role assigned to it. Also, ensure that the service account has the correct Identity Provider information with bound claims.

### Should I just give my service account an owner role?

No, it is not recommended to give a service account an owner role. Generally, a `developer` role is sufficient for most use cases. An owner role will allow the service account to manage the organization, its members, and arbitrarily perform any action. This goes against the principle of least privilege.

### Can a service account from one organization access resources in another organization?

No, a service account can only access resources within the organization it is a member of.

### I don't want to give my organization-scoped service account access to all its projects. What should I do?

Organization-scoped service accounts are not automatically designated as project members. You can manually add an organization-scoped service account to a project as a member and adjust its access by assigning a role to it (e.g., `viewer`).

### Will the CLI periodically renew the token for the service account?

Yes, the CLI will periodically renew the token for the service account. The token should be renewed a short period of time before it expires.

### Why am I seeing a "[subject] is not authorized to perform the requested operation" error?

Generally, this error occurs when the service account is able to view the resource but not modify it. Please reach out to the organization or project owner to get the necessary role assigned to it.

### Why am I seeing a "Resource not found" error?

This error occurs when the service account is not a member of the organization or project. Please make sure that the service account is a member of the organization or project and has the necessary role assigned to it.
