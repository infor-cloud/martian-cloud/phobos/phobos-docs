---
title: Variables
description: "What are variables?"
---

## What are variables?

Variables are a project-level resource that hold data that you can use in your `DEPLOYMENT` and `RUNBOOK` pipelines. Variables can be declared in [pipeline templates](./pipeline_templates.md) and then assigned when creating a pipeline.

Phobos organizes variables into project variable sets, which are blocks of variables that are accessible to resources within your project. Project variable sets are useful because they allow a user to reuse the same pipeline template for different deployments by quickly passing in different values for multiple variables. A user can create different versions (revisions) of project variable sets, which offers more flexibility.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Types of variables

Variables are assigned a pipeline type: `DEPLOYMENT` or `RUNBOOK`. `DEPLOYMENT` variables are either assigned to a specified environment or "All Environments". `RUNBOOK` variables cannot be assigned to an environment.

## Project variable sets

To view a project's variable sets, select **Variables** from the project sidebar. The **Project Variables** details page lists variables by `DEPLOYMENT` and `RUNBOOK`. By default, the latest variable set is listed.

When you make updates to a variable set, a new version, designated as a revision, is created. Prior revisions of variable sets can be accessed via the **Revision** dropdown menu.

Use the search input to find a variable by `key`. For `DEPLOYMENT` variables, use the environment dropdown menu to filter variables by environment.

![Variables - main page ](/img/variables/variables-main-page.png "Variables - main page")

If a project does not have a variable set, when you navigate to **Variables**, you will see the introduction page where you can either select **New Variable** to create a single variable for your variable set, or you can click on the **editor** link to navigate to the editor to create multiple variables in your variable set.

![Variables - get started page ](/img/variables/variables-get-started-page.png "Variables - get started page")

:::important
When you create, edit, or delete a single variable, a new revision of the variable set is immediately created. If you want to make multiple changes to a variable set at once, select the **Editor** tab. In the provided editor, you can update multiple variables and save all changes at once as a new variable set (see section on [`creating and editing multiple variables`](#creating-and-editing-multiple-variables-in-a-variable-set)).
:::

:::important About updating
Only the latest revision of a variable set can be edited. To make changes to a set, make sure the latest revision is being displayed.
:::

### Creating a single variable for a variable set

1. On the **Project Variables** details page, select **New Variable**.
2. In the **Add Variable** dialog, select a **Pipeline Type** for the variable.
3. If the **Pipeline Type** is `DEPLOYMENT`, you may select an environment from the **Environment Scope** dropdown, which lists all available environments within the project. You may also enter an environment name for a non-existent environment. If you don't have an environment in mind, leave the field as **All Environments**.
4. Enter a key and a value for the variable.
5. Click the **Keep open to add another** checkbox to keep the dialog open after you have saved your changes and created a new variable. When the checkbox is selected, after your click **Save** and the variable is created, the dialog will remain open and the input fields will clear so you can immediately create a new variable.

    ![Variables - add variable dialog](/img/variables/variables-add-variable-dialog.png "Variables - add variable dialog")

    :::important Reminder
    When you click **Save**, a new revision will automatically be created.
    :::

6. Click **Save** to create the variable.

### Editing a single variable in a variable set

1. Select the pencil icon for the variable you want to edit.

    ![Variables - edit button](/img/variables/variables-edit-button.png "Variables - edit button")

2. In the **Edit Variable** dialog, you can update the key and value.

3. Click the **Keep open to add another** checkbox to keep the dialog open after you have saved your changes and edited the variable. When the checkbox is selected, after your click **Save** and the variable is edited, the dialog will remain open and the input fields will clear so you can immediately create a new variable.
4. Click **Save** to update the variable.

### Deleting a single variable in a variable set

1. Select the **x** icon for the variable you want to delete.

    ![Variables - delete button](/img/variables/variables-delete-button.png "Variables - delete button")

2. In the **Delete Variable** dialog, select **Delete** to permanently delete the variable. Select **Cancel** to close the dialog without deleting the variable.

    ![Variables - delete dialog](/img/variables/variables-delete-dialog.png "Variables - delete dialog")

### View history of a single variable in a variable set

1. Select the history icon for the target variable.

    ![Variables - history button](/img/variables/variables-history-button.png "Variables - history button")

2. The **Variable History** dialog shows previous versions of the variable beginning with when it was created (listed at the bottom), and it also lists who created version.

    ![Variables - history dialog](/img/variables/variables-history-dialog.png "Variables - history dialog")

    :::tip
    To see the complete history of a variable, ensure you are viewing the latest revision of the project set
    :::

### Creating and editing multiple variables in a variable set

Phobos provides a built-in Monaco editor (the code editor that powers VS Code) to create and edit multiple variables in a variable set at once. Select the **Editor** tab to view the project's `DEPLOYMENT` and `PIPELINE` variables in the editor. If the project has no variable sets, then when you go to **Variables**, you can click on the **editor** link to navigate to the editor to create your first variable set.

When viewing an existing variable set, the latest version is displayed by default. Use the **Revision** dropdown to view other revisions of variable sets.

![Variables - editor](/img/variables/variables-editor.png "Variables - editor")

You can copy an entire project variable set by clicking on the copy icon placed on the top right-hand corner of the editor. This is useful if you wanted to edit a earlier revision of project variable set. Click on the copy icon to copy the entire set. 

![Variables - copy button](/img/variables/variables-copy-button.png "Variables - copy button")

Then select the latest version from the **Revision** dropdown. Paste the variable set into the editor, make any necessary changes, and click **Update** to create a new revision.

### Variable set format

```json
[
     {
        "key": "group_path",
        "value": "org/project_a",
        "pipelineType": "RUNBOOK"
    },
    {
        "key": "module_source",
        "value": "example.com/modules/resources-a/aws",
        "pipelineType": "RUNBOOK"
    },
    {
        "key": "ports",
        "value": [5000, 5001],
        "pipelineType": "DEPLOYMENT",
        "environmentScope": "dev"
    },
    {
        "key": "hcl_example",
        "value": "{ foo = \"bar\" }",
        "pipelineType": "RUNBOOK"
    }
]
```

Above is an example of the variable set format that must be used when creating and editing variables in the editor. The variables are displayed in JSON. A valid variable set comprises an array with objects. Each object represents one variable.
A variable must have the following properties:

- `key`
- `value`
- `pipelineType`: either `DEPLOYMENT` or `RUNBOOK`

The `value` property supports various types, including booleans, objects, arrays of mixed types, and miscellaneous types. In the example, the value for `hcl_example` is an HCL object, enclosed in double quotes and using escape characters to adhere to the JSON format.

`DEPLOYMENT` variables may contain an `environmentScope` property. If a `DEPLOYMENT` variable does not have an assigned `environmentScope`, then the variable will be accessible to all environments within the project. `RUNBOOK` variables cannot have an `environmentScope`.

The editor provides linting to point out syntax that does not adhere to the variable set schema or syntax that is not valid JSON. If the JSON is invalid when you try to create or update a project variable set, warnings will display with information about the error.

![Variables - invalid data](/img/variables/variables-invalid-data.png "Variables - invalid data")

## Variables in pipelines

Variables can be declared in pipeline templates, which are blueprints for creating pipelines. If you are creating a pipeline from a pipeline template that has variables declared and you want to pass in different values, you can set variables directly on the form.

When creating a pipeline, you can use the **Pipeline Variables** table to pass in values for your variables. As another option, you can select a project variable set from the **Variable Set Revision** dropdown.

![Variables - pipeline variables](/img/variables/variables-pipeline-variables.png "Variables - pipeline variables")

Similarly, when creating a release that uses a pipeline template with declared variables, you can use the **Release Variables** table to pass in values for variables declared in the designated pipeline templates. Or you can use a project variable set.

![Variables - release variables](/img/variables/variables-release-variables.png "Variables - release variables")

Go to [Creating a pipeline](./pipelines.md#creating-a-pipeline) and [Creating a release](./releases.md#creating-a-release) for more information on adding project variables when creating a pipeline or a release.

## Frequently Asked Questions (FAQ)

### If I set variables in a pipeline or release and also set variables via a project variable set, which variables will be used?

The variables set within the pipeline or release form will take precedence. After that, the variables in the project variable set will be used. If no variables are set and a project variable set has not been assigned, then default values listed on the pipeline templates will be used.
