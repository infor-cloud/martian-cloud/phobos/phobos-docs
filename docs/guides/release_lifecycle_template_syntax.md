---
title: Release Lifecycle Template Syntax
description: "Learn about the syntax for creating release lifecycle templates in Phobos."
---

The following sections describe the supported fields and types that Phobos supports for release lifecycles.

:::info
All Phobos resources have a Phobos Resource Name (PRN) that uniquely identifies the resource. The PRN is a string that starts with `prn:` followed by the resource type, and/or the organization and project name, unique id, etc. PRNs are accessible via the UI on the details page for the resource. The description column in the tables below will indicate if a field can use the PRN.
:::

## Top-level fields

| Name                                          | Description                                                                                                                                                                                                                           |
| --------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [`stage`](#stage)                             | Defines stages in the release lifecycle template, containing deployments, pre-tasks, and post-tasks.                                                                                                                                  |
| [`stage_order`](#stage_order)                 | Specifies the execution order of stages in the release lifecycle template. By default, the stages will be executed based on the order they appear in the template. (optional)                                                         |
| [`variable`](#variable)                       | **Coming Soon:** Defines variables used within the release, including their type, default value, and name.                                                                        |
| [`plugin`](#plugin)                           | Used to configure the plugins used by the release lifecycle template.                                                                                                                                                                 |
| [`plugin_requirements`](#plugin_requirements) | Defines the source and version for external plugins that need to be imported for the configuration.                                                                                                                                   |
| [`jwt`](#jwt)                                 | Defines a JSON Web Token (JWT), which is used for OIDC (OpenID Connect) authentication with external systems.                                                                                                                         |
| [`volume`](#volume)                           | Defines an external volume that can be mounted into the file system of tasks within the release lifecycle template. (e.g., a directory from a git repo can be mounted as a volume into a task so that the task can access the files). |
| [`vcs_token`](#vcs_token)                     | Enables retrieving a VCS token from the specified VCS provider in Phobos. Supports both OAuth and Personal Access Tokens (PAT).                                                                                                       |


## Top-level field definitions

### `stage`

| Name         | Description                                                                                                         | Supported Fields and Types            |
| ------------ | ------------------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| `pre`        | Defines pre-tasks to be executed before the main deployments in the stage.                                          | [`task`](#task)                       |
| `post`       | Defines post-tasks to be executed after the main deployments in the stage.                                          | [`task`](#task)                       |
| `deployment` | Defines a deployment to be executed within the stage, including approval rules, environment name, and dependencies. | see [`deployment`](#deployment) below |

```hcl title="Stage example"
stage "dev" {
  deployment "dev_environment" {
    environment = "dev"
    when = "manual"
    approval_rules = [
      "prn:approval_rule:example_approval_rule",
      "prn:approval_rule:example_approval_rule_2"
    ]
  }
}
```

### `stage_order`

| Name          | Description                                                   |
| ------------- | ------------------------------------------------------------- |
| `stage_order` | Order of stages in the release lifecycle template. (optional) |

```hcl title="Stage order example"
stage_order = ["dev", "prod", "test"]
```

### `variable` _(Coming Soon)_

| Name      | Description                                | Supported Fields and Types                                                                                 |
| --------- | ------------------------------------------ | ---------------------------------------------------------------------------------------------------------- |
| `type`    | Type of the variable. (optional)           | See [list of supported types](https://developer.hashicorp.com/terraform/language/expressions/types#types). |
| `default` | Default value for the variable. (optional) | expression                                                                                                 |

```hcl title="Variable example"
variable "account_name" {
  type    = string
  default = "account1"
}
```

### `plugin`

| Name   | Description                                                                                                                                                                 |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `body` | Dependent on the plugin type. See [Plugin docs](../plugins/intro.md) for information on supported fields. See the `jwt`, `plugin`, and `plugin_requirements` example below. |

### `plugin_requirements`

| Name      | Description                                                          | Types  |
| --------- | -------------------------------------------------------------------- | ------ |
| `replace` | Used to override the plugin with a local filesystem path. (optional) | string |
| `version` | Version of the plugin. (optional)                                    | string |
| `source`  | Source of the plugin.                                                | string |

### `jwt`

Phobos supports the use of JSON Web Tokens (JWTs), which allows for OIDC authentication, ensuring secure access to external resources. The `jwt` block defines a JWT with an audience and name.

| Name       | Description           | Types  |
| ---------- | --------------------- | ------ |
| `audience` | Audience for the JWT. | string |

The JWT can be used in the `service_account_token` field of a plugin to authenticate with an external system (in this case, Tharsis). In the example below, the `tharsis` plugin uses the `tharsis` JWT to authenticate with the Tharsis API.

<details>
<summary>Click to view `jwt`, `plugin`, and `plugin_requirements` example</summary>

```hcl title="jwt, plugin, and plugin_requirements example"
plugin_requirements {
  tharsis = {
    source  = "martian-cloud/tharsis"
    version = "0.2.0"
  }
}

jwt "tharsis" {
  audience = "tharsis"
}

plugin tharsis {
  api_url               = "https://api.tharsis.example.com"
  service_account_token = jwt.tharsis
  service_account_path  = "example-tharsis-group/example-service-account"
}
```

</details>

#### Bound claims

Phobos supports bound claims, which are claims that are bound to a specific JWT. Bound claims are used to restrict the use of a claim to a specific JWT. Below are a list of bound claims that Phobos supports:

| Claim                   | Description                                       |
| ----------------------- | ------------------------------------------------- |
| `aud`                   | The intended audience of the JWT                  |
| `sub`                   | The PRN of the project                            |
| `project_name`          | The name of the project                           |
| `org_name`              | The name of the organization                      |
| `pipeline_id`           | The unique identifier of the pipeline             |
| `pipeline_type`         | The type of the pipeline                          |
| `is_release`            | Indicates if the pipeline is a release            |
| `release_prn`           | The PRN of the release                            |
| `release_lifecycle_prn` | The PRN of the release lifecycle                  |
| `environment`           | The environment in which the pipeline is executed |

Below is an example of using bound claims with an external system. Two bound claims, `aud` and `org_name`, are attached to a service account within Tharsis.

![Bound claims example](/img/pipelines/pipelines-bound-claims.png "Bound claims example")

### `volume`

Volumes and mount points in Phobos allow you to mount directories from VCS providers into your pipeline. Also, see the [`mount_point`](#mount_point) block for more information and for an additional example.

| Name          | Description                      | Supported Fields and Types                                                                                  |
| ------------- | -------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `vcs_options` | VCS options for the volume type. | `ref` (string, optional), `repository_path` (string), `provider_id` (id or prn of the VCS provider; string) |
| `type`        | Type of the volume.              | string                                                                                                      |

```hcl title="volume example"
volume "tools" {
  type = "vcs"
  vcs_options {
    provider_id     = "vcs_provider_id"
    repository_path = "martian/tools"
  }
}
```

### `vcs_token`

The `vcs_token` block is used to retrieve a VCS token from the specified VCS provider in Phobos. The `vcs_token` block supports both OAuth and Personal Access Tokens (PAT). This eliminates the need to store sensitive tokens in the lifecycle template or pass them as variables. This token can be used to initialize a GitLab plugin, for example.

| Name          | Description                              | Supported Fields and Types |
| ------------- | ---------------------------------------- | -------------------------- |
| `provider_id` | ID or PRN of the VCS provider in Phobos. | string                     |

This block will expose the following fields for use in the lifecycle template:

| Name        | Description                                                                                                                                               | Types  |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| `file_path` | Path to the token file. (e.g., `vcs_token.gitlab_token.file_path`). The token in the file will be updated automatically before expiration, if applicable. | string |
| `value`     | Static token value. (e.g., `vcs_token.gitlab_token.value`). This value will not be updated.                                                               | string |

```hcl title="vcs_token example"
vcs_token "gitlab_token" {
  provider_id = "prn:vcs_provider:my-org/gitlab"
}

plugin_requirements {
  gitlab = {
    source  = "martian-cloud/gitlab"
  }
}

plugin gitlab {
  api_url    = "https://gitlab.example.com/api/v4"
  token_file = vcs_token.gitlab_token.file_path
  auth_type  = "oauth_token"
}
```

In the example above, the `vcs_token` block retrieves a token from the GitLab VCS provider in Phobos. The `gitlab` plugin uses this token to authenticate with the GitLab API. The `token` field in the `gitlab` plugin is set to the `file_path` of the `gitlab_token` VCS token, ensuring the plugin uses the latest token before expiration. The `auth_type` field is set to `oauth_token`, indicating that the token is an OAuth token. Refer to the plugin's documentation for more details on supported authentication methods.

:::caution tokens are sensitive
Ensure that the token is not exposed in the pipeline template or in the logs. The token should be stored securely and not shared with unauthorized users. A feature to mask sensitive data in the logs may be available in future releases.
:::

## Sub-level fields and definitions

### `task`

Tasks are one of the two fundamental units of work in a stage (the other being pipelines). Tasks utilize the plugin framework, which gives tasks access to a plugin's actions (see [Plugins](../plugins/intro.md)).

A task is executed automatically by default when it's ready; however, a task can also be manual in which case it would need to be manually started. A task can also act as a gate by setting the `interval` and `success_condition` fields. When the `interval` field is set, the task will be executed periodically until either the success condition has been satisfied or max attempt limit has been reached.

A task can also define a list of `approval_rules`, which specifies the members who can approve the task. Learn more about [Approval Rules](approval_rules.md).

| Name                | Description                                                                           | Supported Fields and Types                        |
| ------------------- | ------------------------------------------------------------------------------------- | ------------------------------------------------- |
| `dependencies`      | Defines dependencies for the task.                                                    | array of strings, optional                        |
| `success_condition` | Condition that determines if the task was successful.                                 | expression, optional                              |
| `approval_rules`    | Unique identifiers or PRNs for approval rules for the task.                           | array of strings, optional                        |
| `when`              | Defines when the task should be executed (auto or manual).                            | string, optional                                  |
| `interval`          | Interval for repeatable tasks.                                                        | string, optional                                  |
| `attempts`          | Number of attempts for the task.                                                      | integer, required if `interval` is set            |
| `action`            | Actions to be executed within the task.                                               | see [`action`](#action) below, optional           |
| `mount_point`       | Mount points for volumes within the task.                                             | see [`mount_point`](#mount_point) below, optional |
| `schedule`          | Schedule the task's start time using a datetime or cron expression                    | [`schedule`](#schedule), optional                 |
| `if`                | Condition to execute the task. When the condition is false, the task will be skipped. | expression, optional                              |

### `deployment`

Deployments are used to define the environment for the release lifecycle template. Deployments can have approval rules, dependencies, and a `when` field that defines when the deployment should be executed.

| Name             | Description                                                                                       | Supported Fields and Types        |
| ---------------- | ------------------------------------------------------------------------------------------------- | --------------------------------- |
| `approval_rules` | Unique identifiers or PRNs for approval rules for the task.                                       | array of strings, optional        |
| `when`           | Defines when the task should be executed (auto or manual).                                        | string, optional                  |
| `environment`    | The name of the environment for the deployment.                                                   | string                            |
| `dependencies`   | List of other deployments that this deployment depends on.                                        | array of strings, optional        |
| `schedule`       | Schedule the deployment's start time using a datetime or cron expression                          | [`schedule`](#schedule), optional |
| `if`             | Condition to execute the deployment. When the condition is false, the deployment will be skipped. | expression, optional              |

```hcl title="Deployment example"
stage "dev" {
  deployment "dev_environment" {
    environment = "dev"
    when = "manual"
    approval_rules = [
      "prn:approval_rule:example_approval_rule",
      "prn:approval_rule:example_approval_rule_2"
    ]
  }
}
```

### `action`

| Name    | Description                                                                              | Types            |
| ------- | ---------------------------------------------------------------------------------------- | ---------------- |
| `alias` | Alias for the action. (optional)                                                         | string, optional |
| `label` | Label for the action in the format `"plugin-name_action-name"` (e.g., `"exec_command"`). | string           |

### `mount_point`

Mount points and volumes in Phobos allow you to mount directories from VCS providers into your pipeline. See the [`volume`](#volume) block above for more information and for an additional example.

| Name     | Description                                       | Types  |
| -------- | ------------------------------------------------- | ------ |
| `path`   | Path where the volume will be mounted. (optional) | string |
| `volume` | Volume to be mounted.                             | string |

```hcl title="Task, action, and mount point example"
plugin "exec" {}

volume "demo_project" {
  type = "vcs"
  vcs_options {
    provider_id     = "prn:vcs_provider:example-provider"
    ref             = "main"
    repository_path = "path/to/repo"
  }
}

stage "security_scans" {
  task "scan" {
    mount_point {
      volume = "demo_project"
      path   = "/"
    }
    action "exec_command" {
      command = <<EOF
        echo "Simulated security scan by listing files in directory"
        ls -la
        sleep 5
      EOF
    }
  }
}
```

In the example above, the `scan` task mounts the `demo_project` volume to the root directory. The `exec_command` action lists the files in the directory and sleeps for 5 seconds.

### `schedule`

The schedule block is used to set a datetime or cron schedule for a deployment/task. When specifying the schedule block, the `when` field for the deployment or task node must be set to `auto` since the deployment/task will be automatically started based on the specified schedule once the deployment/task moves out of the `BLOCKED` state. The schedule can also be updated after the pipeline has been created via the API/UI.

| Name      | Description                                                                    | Types  |
| --------- | ------------------------------------------------------------------------------ | ------ |
| `type`    | The type of schedule which can be set to `datetime` or `cron`                  | string |
| `options` | The options are specific to the type of schedule specified by the `type` field | object |

#### Datetime schedule type

The `datetime` schedule type requires a timestamp in RFC3339 format. When the deployment/task moves out of the `BLOCKED` state it'll automatically be scheduled to start at the specified time, if the datetime is in the past then the deployment/task will start immediately.

#### Cron schedule type

The `cron` schedule type requires a cron expression and timezone in order to calcuate the scheduled start time. When the deployment/task moves out of the `BLOCKED` state, the cron expression will be evaluated to determine what the scheduled start time will be. It's important to note that the cron expression does not get evaluated until the deployment/task node transitions out of the `BLOCKED` state. The cron schedule is useful when scheduling deployments for a specific maintenance window which
occurs on a standard cadence.

NOTE: The cron schedule is only used to calculate the scheduled start time for the deployment/task node and does not cause it to run periodically (i.e. the deployment/task will only run a singe time)

```hcl title="Schedule example"
stage "dev" {
  deployment {
    when = "auto"
    schedule {
      type = "datetime"
      options = {
        value = "2024-11-25T14:50:00.000Z"
      }
    }
    environment = "dev-us-east-1"
  }

  deployment {
    when = "auto"
    schedule {
      type = "cron"
      options = {
        expression = "* * * * 5#3"
        timezone   = "America/New_York"
      }
    }
    environment = "dev-us-east-2"
  }
}
```

In the example above, the first deployment is scheduled using a specific datetime in RFC3339 format. The second deployment is scheduled using a cron expression which will set the scheduled start time to 12AM on the 3rd Friday of the Month in the America/New_York timezone.
