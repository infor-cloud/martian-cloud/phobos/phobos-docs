---
title: Approval Rules
description: "What are approval rules and how are they used in Phobos?"
---

## What are approval rules?

Approval rules are a way to manage the flow of pipeline tasks and nested pipelines. An approval rule consists of designated _approvers_ (users, teams, or service accounts) and a required number of approvers who must approve a task or nested pipeline before it can proceed.

When an approval rule is added to a task or nested pipeline, it will not execute until the required number of approvers in the rule have given their approval. One or more approval rules can be added. The task or nested pipeline will not execute until all approval rules have been satisfied.

Approval rules can be added to pipeline templates where they can manage specific tasks or nested pipelines. When creating a release, they can also be added to release lifecycle templates to require approval before deploying to an environment.

Approval rules can be created at the organization level or at the project level. When an approval rule is created at the organization level, it is available to all projects within the organization. When an approval rule is created at the project level, it is only available to the project in which it was created.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Pipeline template example

```hcl title="pipeline_template.hcl"
plugin exec {}

stage "example" {
  task "test_task" {
    approval_rules = [
        "prn:approval_rule:org_1/admins",
        "prn:approval_rule:org_1/project_a/deployers"
        ]
    action "exec_command" {
      command = <<EOF
        echo "Testing a simple command in the pipeline"
      EOF
    }
  }
}
```

In the pipeline template above, the task `test_task` has two approval rules, identified by their PRNs: `admins` and `deployers`.

When we run this pipeline, we can navigate to the task details page in the UI. We see the two approval rules. We see **Approve** which means we are one of the designated approvers on at least one of the approval rules.

![Approval Rules - approve tasks](/img/approval_rules/approval-rules-approve-tasks.png "Approve tasks")

After approving the task, the UI will update to show that we have approved the task and will display any remaining approvals left. If not all approvals are met, we have the option to **Revoke Approval**.

![Approval Rules - revoke approval](/img/approval_rules/approval-rules-revoke-approval.png "Revoke Approval")

## Release lifecycle template example

```hcl title="release_lifecycle_template.hcl"
stage "dev" {
  deployment "dev_environment" {
    environment = "dev"
    approval_rules = [
      "prn:approval_rule:org_1/admins",
      "prn:approval_rule:org_1/project_b/reviewers"
    ]
  }
}
```

In the release lifecycle template above, the deployment `dev_environment` has two approval rules, identified by their PRNs: `admins` and `reviewers`.

When we create a release using this release lifecycle template, it creates a pipeline of type `RELEASE_LIFECYCLE` and a pipeline of type `DEPLOYMENT`. Note, the `DEPLOYMENT` pipeline has a status of `APPROVAL PENDING` because it has approval rules that have not been met.

![Approval Rules - pipelines](/img/approval_rules/approval-rules-pipelines.png "Approval Rules - pipelines")

When we navigate to the `DEPLOYMENT` pipeline's details page, we see the two approval rules. We see **Approve** which means we are one of the designated approvers on at least one of the approval rules.

![Approval Rules - approve pipeline](/img/approval_rules/approval-rules-approve-pipeline.png "Approve pipeline")

## To-do items

When a pipeline task or nested pipeline with an approval rule is created, a to-do item is generated. Users who are designated as approvers will receive a notification that they must approve the to-do item.

The list icon in the navigation bar will display a badge with the number of to-do items that require your attention. Click on the icon to view your **To-Do List**.

On the **To-Do List** page, the **To Do** tab displays to-do items that require your attention. The **Resolved** tab displays to-do items that you have approved. Click on each to-do item to view the task or pipeline details page where you can approve or revoke the approval, or review the task or pipeline.

![Approval Rules - to-do items list](/img/approval_rules/approval-rules-to-do-items-list.png "Approval Rules - to-do items list")

## Viewing approval rules

To view the approval rules, select **Approval Rules** from the sidebar. The approval rules page displays a list of all approval rules in the organization or project. The list of project approval rules will also display the approval rules from the parent organization, which are noted with the `inherited from org` chip.

![Approval Rules - list](/img/approval_rules/approval-rules-list.png "Approval Rules - list")

## Creating an approval rule

1. Go to the sidebar and select **Approval Rules**.
2. Click **Add Approval Rule**.
3. Enter a name for the approval rule.
4. Enter a description for the approval rule (optional).
5. Select a number for **Approvals Required**.

:::important
**Approvals Required** is the number of approvers who must approve the task before the task can proceed. Once this number is set, it cannot be changed.
:::

6. Select approvers from the **Add Approvers** dropdown. This dropdown lists all users, teams, and service accounts in the organization or project. You should select the same or a higher number of approvers as the number of approvals required.
7. Click **Create Approval Rule**.

:::important Editing and deleting approval rules
Organization-scoped approval rules can only be edited or deleted at the organization level. Project-scoped approval rules can only be edited or deleted at the project level.
:::

## Editing an approval rule

1. Go to the sidebar and select **Approval Rules**.
2. Click on the pencil icon next to the approval rule you want to edit.
3. Edit the description.
4. Add approvers from the **Add Approvers** dropdown. You can also remove current approvers by clicking on the trash icon next to their name.
5. Click **Update Approval Rule** to save your changes. Click **Cancel** to discard your changes and return the list of approval rules.

## Deleting an approval rule

1. Go to the sidebar and select **Approval Rules**.
2. Click on the **X** icon next to the approval rule you want to delete.
3. In the confirmation dialog, click **Delete** to confirm the deletion. Click **Cancel** to keep the approval rule.

## Frequently Asked Questions (FAQ)

### What's the difference between an approval rule and an environment protection rule?

Approval rules designate who can approve a pipeline task or nested pipeline before it can proceed. They are designed to be placed within templates to manage the flow of tasks or nested pipelines. Approval rules generate a to-do item; users will receive a notification that they must approve the to-do item.

Environment protection rules assign which principals or roles can deploy to a specified environment. They can be thought of as blanket rules that ensure that only authorized principals or members with specified roles can deploy to certain environments.

Additionally, an approval rule must be added to a template for it to take effect, in contrast to environment protection rules, which when created are automatically applied to all deployments in the specified environment. Learn more about [Environment Protection Rules](/docs/guides/environments.md#environment-protection-rules).

### If I'm creating a release, should I place my approval rule in the release lifecycle template or the pipeline template?

It depends on the configuration of your project, but generally, if you want to require approval for a deployment to a specific environment, you should place the approval rule in the release lifecycle template. If you want to require approval for a specific task or nested pipeline, you should place the approval rule in the pipeline template.
