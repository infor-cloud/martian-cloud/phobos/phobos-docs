---
title: Pipeline Templates
description: "What are pipeline templates?"
---

## What are pipeline templates?

Pipeline templates are the blueprints for creating pipelines. Pipeline templates are defined by utilizing HCL (HashiCorp Configuration Language) and use semantic versioning.

Pipeline templates can be stored and managed within Phobos, or they can be stored as HCL files in your VCS provider where pipelines can be created from them.

Pipeline templates are reusable and can be used to create multiple pipelines. Versioning pipelines allows users to track changes and understand the nature of the changes made to a pipeline.

:::important
Phobos leverages HCL to construct pipeline templates. Go to the [Pipeline Template Syntax](./pipeline_template_syntax.md) page for details on writing pipeline templates and using the associated blocks and fields. You can also learn more about HCL [here](https://developer.hashicorp.com/terraform/language/syntax/configuration).
:::

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Pipeline template example

```hcl
variable "sample" {
  type    = string
  default = "this is the default value"
}

variable "ports" {
  type    = list(number)
  default = [8000, 8001]
}

plugin exec {}

stage "deployment" {
  task "deploy_resource" {
    action "exec_command" {
      command = <<EOF
          echo "Deploying resource..."
          echo "value for sample=${var.sample}"
          echo "value for ports=${jsonencode(var.ports)}"
          echo "Resource has been deployed successfully."
      EOF
    }
  }
}
```

Above is example of a simple and valid pipeline template. The example uses the `exec` plugin, which is a built-in Phobos plugin that executes shell commands (more on [`exec`](../plugins/exec.md)).

It contains a `stage` block, named `deployment`. The `stage` block contains a `task` block, named `deploy_resource`. The `task` block contains an `action` block, named `exec_command`. The `action` block contains a `command` field, which is a shell script that will be executed when the pipeline is run.

Two `variable` blocks are defined at the top of the template. When generating a pipeline from this template, the user can provide values for these variables and if not provided, the default values will be used.

## Viewing and searching pipeline templates

Go to the project details page and select **Pipeline Templates** from the sidebar to view and search for available pipeline templates. Selecting a pipeline template will navigate you to its details page.

## Versioned and non-versioned pipeline templates

Pipeline templates can be _versioned_ or _non-versioned_.

Versioned pipeline templates have names and use semantic versioning, which allow users to track and understand the nature of changes to a pipeline. The version number comprises three parts: major, minor, and patch. For example, a change from `1.0.0` to `1.0.1` indicates a bug fix, while a change from `1.0.0` to `2.0.0` indicates significant changes. Learn more about Semantic Versioning [here](https://semver.org/).

Non-versioned pipeline templates do not have names and do not use semantic versioning. When creating a pipeline via the CLI using the `pipeline create` command, a non-versioned pipeline template will be created.

Non-versioned templates are not displayed in **Pipeline Templates**, which only lists versioned pipeline templates. However, if a pipeline was created from a non-versioned template, you can go to the pipeline details page where you will find a link to the pipeline template. Additionally, creation of a non-versioned pipeline template will show up in the activity feeds.

## Pipeline template details page

### Versioned

The details page for a versioned pipeline template contains the following sections:

- **Template**: the pipeline template code
- **Versions**: a list of all versions of the pipeline template

By default, the latest version of the pipeline template is displayed in the **Template** tab. You can select a different version from the **Versions** tab. Selecting a different version will display the template code for that version. A **latest** badge is displayed next to the latest version of the pipeline template.

The pipeline template sidebar displays the active version of the pipeline template and as well as who created the version and when it was created.

![Pipeline template details page - versioned](/img/pipeline_templates/pipeline-template-details-versioned.png "Pipeline template details page - versioned")

### Non-versioned

The details page for a non-versioned pipeline template contains the **Template**, which displays the pipeline template code. Note that the non-versioned template does not have a **Versions** tab, a name, nor an option to create a new version.

![Pipeline template details page - non-versioned](/img/pipeline_templates/pipeline-template-details-non-versioned.png "Pipeline template details page - non-versioned")

## Creating a pipeline template

1. Navigate to the target project where the pipeline template will be created and select **Pipeline Templates** from the sidebar.
2. Select **New Pipeline Template**.
3. Enter a name for the pipeline template.
4. Enter a semantic version for the pipeline template.
5. Enter the pipeline template code. You can enter HCL code directly into the code editor provided or you may upload the pipeline template as an HCL file.
6. Select **Create Pipeline Template**.

## Creating a new version of a pipeline template

1. Navigate to the target pipeline template and select **Create New Version**.
2. Enter a semantic version for the new pipeline template version.
3. Enter the pipeline template code. The previous pipeline template code will be displayed in the code editor. You can make changes to the code or upload a new HCL file.
4. Select **Create New Version**.

## Deleting a pipeline template

1. Navigate to the target pipeline template and click on the upside-down caret next to the **Create New Version** button. Select **Delete Pipeline**.
2. In the **Delete Pipeline Template** dialog, select **Delete** to confirm deletion. Select **Cancel** to keep the pipeline template.

## Frequently Asked Questions (FAQ)

### Can I update a pipeline template?

Once a version of a pipeline template is created, it cannot be updated. However, you can create a new version of the pipeline template with the desired changes.
