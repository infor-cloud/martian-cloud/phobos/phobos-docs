---
title: Projects
description: "What projects help us accomplish"
---

## What are projects?

Projects are contained within [organizations](./organizations.md) and provide many of the resources necessary to deploy a specific application or product. They have the flexibility to contain your entire application or one component of your application.

Projects are where [releases](./releases.md) are managed and executed. They also contain [pipelines](./pipelines.md), which are the core unit of automation in Phobos.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Project resources

Projects contain the following resources:

- [Releases](./releases.md)
- [Release Lifecycles](./release_lifecycles.md)
- [Pipelines](./pipelines.md)
- [Pipeline Templates](./pipeline_templates.md)
- [Environments](./environments.md)
- [Approval Rules](./approval_rules.md)
- [VCS Providers](./vcs_providers.md)

## Viewing and searching projects

Go to the organization details page to view and search for available projects. Selecting a project will navigate you to its details page.

## Project details page

The project details page provides an overview of the project and contains the following sections:

- **Activity**: A feed that displays all the activity associated with the project. Clicking **My Activity** will filter the feed to show only your activity.
- **To-Do Items**: List of to-do items within the project that require your attention.
- **Environments**: List of the environments within the project, including the most recently completed deployment in each environment.
- **Pipelines**: List of the most recent pipelines, including the status of each pipeline.

![Project detail page](/img/projects/project-detail-page.png "Project detail page")

## Project memberships

Memberships are used to manage access to projects. Each membership is associated with a role that defines what the user can do within the project.

Organization memberships are inherited by all projects within the organization. Additionally, project memberships can be created to provide access to specific projects.

Go to **Members** in the sidebar to view and manage the project's memberships. Memberships include those inherited from the organization and project-specific memberships. From here, you can add, update, or delete project memberships.

Learn more about [memberships](./memberships.md).

## Creating a project

1. Select the target organization in which the project will be created.
2. Select **New Project**.
3. Enter a name for the project and a description (optional).
4. Select **Create Project**.

## Updating a project

1. Select the target project.
2. Select **Settings** from the sidebar.
3. Update the project's description.
4. Select **Save Changes**.

## Deleting a project

1. Select the target project.
2. Select **Settings**.
3. Select **Delete Project**.
4. When the confirmation dialog appears, type or copy and paste the project's name to confirm deletion.
5. Select **Delete**.

:::danger deletion is dangerous
Deleting a project is an <u>**irreversible**</u> operation.

Proceed with **extreme** caution as deletion **permanently** removes <u>**ALL**</u> resources within the project, including all pipelines and releases. If unsure, **do not** proceed.
:::

## Frequently Asked Questions (FAQ)

### Who can create, update, and delete a project?

System administrators and organization members with the role of owner can create, update, and delete projects. Project owners can update projects.
