---
title: Organizations
description: "What are organizations, and why are they important?"
---

## What are organizations?

Organizations are the top-level isolation boundary in Phobos. They contain [projects](./projects.md), where entire applications or components of applications are managed and released.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Organization structure

Organizations enable Phobos to use a single instance for multiple groups or customers (i.e., multitenancy). Each organization is completely isolated from other organizations, and only system administrators can create new organizations. Access to an organization is controlled using role-based access control (see [Organization memberships](#organization-memberships)).

The structure of an organization is dependent on your requirements. For example, you can create an organization for each customer, team, or group. Each organization can contain one or more projects, which can be used to manage different applications or components.

Organizations include the following resources:

- [Release Lifecycles](./release_lifecycles.md)
- [Service Accounts](./service_accounts.md)
- [Approval Rules](./approval_rules.md)
- [Agents](./agents.md)
- [VCS Providers](./vcs_providers.md)
- [Plugins](../plugins/intro.md)

## Viewing and searching organizations

The home dashboard (the main page for Phobos) displays all the activities for the resources that are available to the user, including organizations to which the user has access.

![Home dashboard](/img/organizations/home-dashboard.png "Home dashboard")

Phobos users can view and search for available organizations from the **Organizations** dropdown in the navigation bar.

![Organization dropdown](/img/organizations/organization-dropdown.png "Organization dropdown")

System administrators have access to the **Admin Area** where they can view and search for all organizations. System administrators can access the **Admin Area** by clicking on their user avatar in the top right corner and selecting **Admin Area**.

![Admin Area](/img/organizations/admin-area-avatar.png "Admin Area")

## Organization details page

Selecting an organization will navigate you to its details page. Here you can view the organization's name, description, and its projects. From the sidebar, you can navigate to the organization's resources.

![Organization detail page](/img/organizations/organization-detail-page.png "Organization detail page")

## Organization memberships

Memberships are used to manage access to organizations. Each membership is associated with a role that defines what the user can do within the organization.

Organization memberships are inherited by all projects within the organization.

Select **Members** in the sidebar to view and manage the organization's memberships. From here, you can add, update, or delete memberships.

Learn more about [memberships](./memberships.md).

## Creating an organization

1. Select **Admin Area > Organizations**.
2. Select **New Organization**.
3. Enter a name for the organization and a description (optional).
4. Select **Create Organization**.

## Updating an organization

1. Select the target organization.
2. Select **Settings** from the sidebar.
3. Update the organization's description.
4. Select **Save Changes**.

## Deleting an organization

1. Select the target organization.
2. Select **Settings**.
3. Select **Delete Organization**.
4. When the confirmation dialog appears, type or copy and paste the organization's name to confirm deletion.
5. Select **Delete**.

:::danger deletion is dangerous
Deleting an organization is an <u>**irreversible**</u> operation.

Proceed with **extreme** caution as deletion **permanently** removes <u>**ALL**</u> projects, including all resources associated with the organization. If unsure, **do not** proceed.
:::

## Frequently Asked Questions (FAQ)

### Who can create, update, and delete an organization?

Only system administrators can create and delete organizations. System administrators and organization owners can update organizations.

### Can I create multiple organizations? Or can I create nested organizations?

You can create multiple organizations. You cannot create nested organizations because they are designed to be a top-level entity. You can create multiple projects within an organization to manage different applications or components.

### Why can't I see or access an organization?

You may not have access to the organization. Contact the organization owner or a system administrator to obtain access.

### Why can't I see the Admin Area?

Only system administrators have access to the Admin Area. Contact your system administrator to obtain access.
