---
title: Introduction
description: "Get started with the Phobos CLI."
---

The CLI is a powerful tool for interacting with Phobos. It allows us to create and manage pipelines, projects, and much more. This guide will help us get started with the Phobos CLI.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Authentication

- The CLI supports two authentication methods:
  1. [Single Sign-On (SSO)](#single-sign-on-sso)
  2. [Environment variables](#environment-variables)

### Single Sign-On (SSO)

SSO authentication is meant to be used by human users.

- When using SSO, the CLI...
  1. temporarily opens up a web browser session.
  2. gets a token from the configured sign-in provider via a callback server.
  3. stores that in a separate credentials file.
  4. uses the token to authenticate against the Phobos API for every request.

:::note
The token must be periodically renewed via the SSO process.  
It is recommended that the token be renewed before running a long job to avoid expiration mid-run.
:::

### Environment Variables

Environment variables are meant to be used for CI/CD pipelines and other automated processes.

#### Service Account

Service accounts are meant to be used for Machine to Machine (M2M) authentication like a CI/CD pipeline.

| Name                           | Description                                         |
| ------------------------------ | --------------------------------------------------- |
| `PHOBOS_SERVICE_ACCOUNT_PATH`  | The full path to the service account within Phobos. |
| `PHOBOS_SERVICE_ACCOUNT_TOKEN` | The token to be used for authentication.            |
| `PHOBOS_HTTP_ENDPOINT`         | The full URI to the Phobos API instance.            |

:::note important
Environment variables for service accounts take precedence over SSO authentication.
:::

#### Static Token

A static token is a user or service account token that will be used to directly authenticate with the API.

| Name                     | Description                              |
| ------------------------ | ---------------------------------------- |
| `PHOBOS_STATIC_TOKEN`    | The token to be used for authentication. |
| `PHOBOS_HTTP_ENDPOINT`   | The full URI to the Phobos API instance. |
| `PHOBOS_TLS_SKIP_VERIFY` | Set to `true` to skip TLS verification.  |

:::note important
Environment variables for static tokens take precedence over SSO authentication.
:::

## Profiles

The CLI includes a profile system making it possible to easily switch back-and-forth between different Phobos instances.

All profiles are stored under the user's home directory inside a `.phobos` directory. The `settings.json` file contains the client settings and `credentials.json` contains the SSO tokens for every profile. The CLI will automatically create the directory and the default profile upon the configuration of the first profile.

:::info
The profile is a global option meaning a user can simply switch to another profile with just an option!
:::

## Frequently Asked Questions (FAQ)

### How do I create a new profile?

Use the `phobos configure` command to create a new profile.

### How do I list all profiles?

Use the `phobos configure list` command.

### How do I switch to another profile?

Use the `-p [name]` option before the subcommand to switch to another profile.
