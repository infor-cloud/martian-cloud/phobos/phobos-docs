---
title: API
description: "An installation and build guide for the Phobos API"
---

At the moment, the Phobos API does not provide any binaries. However, it is possible to run it locally with the following instructions.

## Build and run locally (Docker)

:::tip
Even easier, use the provided Docker images. Learn [more](docker.md).
:::

### Requirements

- [Go](https://go.dev/doc/install)
- [Make](https://www.gnu.org/software/make/)
- Optional Go packages for linting and testing:
  ```bash showLineNumbers
  go install golang.org/x/lint/golint@latest
  go install github.com/vektra/mockery/v2@latest
  ```

### Build from source (Docker)

```bash title="Git clone the project to the local machine"
git clone https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api.git
```

```bash showLineNumbers title="Start the PostgreSQL database"
cd <project-directory>
make db-start
```

```bash title="Prepare the database"
make migrate
```

```bash title="Build the API binary"
make build-api
```

```bash showLineNumbers title="Build the API Docker image and run it"
make build-docker
make run-docker
```

## Environment variables

<details>
<summary>Expand for a list and explanation</summary>

|                                                    Name |        Generic Value        | Description                                                      |
| ------------------------------------------------------: | :-------------------------: | ---------------------------------------------------------------- |
|                   `PHOBOS_OAUTH_PROVIDERS_0_ISSUER_URL` |              -              | Configured OIDC provider's issuer URL.                           |
|                    `PHOBOS_OAUTH_PROVIDERS_0_CLIENT_ID` |              -              | Client ID for identity provider.                                 |
|               `PHOBOS_OAUTH_PROVIDERS_0_USERNAME_CLAIM` |              -              | Supported username claim.                                        |
|                        `PHOBOS_OAUTH_PROVIDERS_0_SCOPE` |              -              | OAuth scopes used by the Phobos UI.                              |
|                   `PHOBOS_OAUTH_PROVIDERS_0_LOGOUT_URL` |              -              | Logout URL for the Phobos UI.                                    |
|                               `PHOBOS_ADMIN_USER_EMAIL` |              -              | Email for the default API admin user if one is to be created.    |
|                                    `PHOBOS_DB_PASSWORD` |          postgres           | The clear-text password for the PostgreSQL database.             |
|                                    `PHOBOS_DB_USERNAME` |          postgres           | The username for the PostgreSQL database.                        |
|                              `PHOBOS_DB_PASSWORD_CRYPT` |              -              | Encrypted version of the password for the PostgreSQL database.   |
|                                `PHOBOS_DB_PASSWORD_ARN` |            none             | The Amazon Resource Number (ARN) where DB password is stored.    |
|                                        `PHOBOS_DB_NAME` |           phobos            | Username for PostgreSQL database.                                |
|                                        `PHOBOS_DB_HOST` |          localhost          | Host address the database container binds to.                    |
|                                        `PHOBOS_DB_PORT` |            5432             | Port number where API connects with the database.                |
|                                    `PHOBOS_DB_SSL_MODE` |            false            | Boolean indicating if database uses SSL.                         |
|                       `PHOBOS_OBJECT_STORE_PLUGIN_TYPE` |           aws_s3            | Object store plugin type.                                        |
|                `PHOBOS_OBJECT_STORE_PLUGIN_DATA_BUCKET` |              -              | Name of the bucket where objects will be stored.                 |
|                `PHOBOS_OBJECT_STORE_PLUGIN_DATA_REGION` |              -              | Region where the object store is hosted.                         |
|     `PHOBOS_OBJECT_STORE_PLUGIN_DATA_AWS_ACCESS_KEY_ID` |              -              | AWS access key ID used to access the object store.               |
| `PHOBOS_OBJECT_STORE_PLUGIN_DATA_AWS_SECRET_ACCESS_KEY` |              -              | AWS secret access key used to access the object store.           |
|              `PHOBOS_OBJECT_STORE_PLUGIN_DATA_ENDPOINT` |    http://localhost:9002    | URL to the object store.                                         |
|                       `PHOBOS_JWS_PROVIDER_PLUGIN_TYPE` |              -              | JSON Web Signature (JWS) provider plugin type.                   |
|                `PHOBOS_JWS_PROVIDER_PLUGIN_DATA_KEY_ID` |              -              | JWS provider key ID.                                             |
|                `PHOBOS_JWS_PROVIDER_PLUGIN_DATA_REGION` |              -              | Region where JWS plugin is hosted.                               |
|                     `PHOBOS_JOB_DISPATCHER_PLUGIN_TYPE` |           docker            | Type of job executor plugin: kubernetes, ecs, docker, local.     |
|             `PHOBOS_JOB_DISPATCHER_PLUGIN_DATA_API_URL` |    http://localhost:9000    | Job dispatcher API URL.                                          |
|                `PHOBOS_JOB_DISPATCHER_PLUGIN_DATA_HOST` | unix:///var/run/docker.sock | Host for the job dispatcher.                                     |
|         `PHOBOS_JOB_DISPATCHER_PLUGIN_DATA_EXTRA_HOSTS` |              -              | Extra hosts for job executor docker configuration.               |
|               `PHOBOS_JOB_DISPATCHER_PLUGIN_DATA_IMAGE` |              -              | Docker image used for the job executor.                          |
|         `PHOBOS_JOB_DISPATCHER_PLUGIN_DATA_LOCAL_IMAGE` |            true             | Boolean specifying if job executor image is using a local image. |
|                                        `PHOBOS_API_URL` |    http://localhost:9000    | Endpoint where the Phobos API will be accessible.                |
|                     `PHOBOS_SERVICE_ACCOUNT_ISSUER_URL` |    http://localhost:9000    | Issuer URL for Phobos service account authentication.            |
|                             `PHOBOS_OTEL_TRACE_ENABLED` |            true             | Boolean specifying whether tracing is enabled.                   |
|                                `PHOBOS_OTEL_TRACE_TYPE` |        otlp or xray         | Type of tracing data to send.                                    |
|                                `PHOBOS_OTEL_TRACE_HOST` |              -              | Host or IP address to send trace data to.                        |
|                                `PHOBOS_OTEL_TRACE_PORT` |            4317             | Port to send trace data to.                                      |
|                                       `HTTP_RATE_LIMIT` |             60              | HTTP requests per second allowed by the rate limiter             |

</details>

:::note
If running within WSL using Docker Desktop it might be necessary to change: `PHOBOS_DB_HOST=kubernetes.docker.internal`.
:::

## Supported databases

The Phobos API requires a database to store persistent information such as, organizations, projects, users, etc. It currently supports [PostgreSQL](https://www.postgresql.org/).

## Open Telemetry (OTel) tracing

The Phobos API supports Open Telemetry (OTel) tracing. To enable it, set the following environment variables. The list above has more information about the variables.

- `PHOBOS_OTEL_TRACE_ENABLED`
- `PHOBOS_OTEL_TRACE_TYPE`
- `PHOBOS_OTEL_TRACE_HOST`
- `PHOBOS_OTEL_TRACE_PORT`

## Rate limit

The Phobos API rate limits inbound HTTP requests for GraphQL queries and mutations. Environment variable `HTTP_RATE_LIMIT` specifies the number of requests per second allowed by the rate limiter. The default is 60.

## Resource limits

The Phobos API limits the numbers of certain resources in order to avoid performance breakdown or other problems. The default values are intended to not be overly restrictive. The current active limit values can be seen via this GraphQL query:

```graphql title="Get the current resource limits" showLineNumbers
query {
  resourceLimits {
    id
    name
    value
  }
}
```

A specific limit's value can be modified via a GraphQL mutation similar to this:

```graphql title="Update the limit for the number of managed identities per workspace" showLineNumbers
mutation {
  updateResourceLimit(
    input: {
      name: "ResourceLimitAssignedManagedIdentitiesPerWorkspace"
      value: 45
    }
  ) {
    resourceLimit {
      id
      name
      value
    }
  }
}
```
