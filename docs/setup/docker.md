---
title: Docker
description: "Running Phobos using provided Docker images"
---

If you wish to try out the Phobos suite in Docker, we provide a Docker Compose file that includes all the necessary services to run Phobos. This includes the API, UI, Keycloak, Postgres, and Minio. The Docker Compose file is meant for development and testing purposes only and should not be used in production.

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Docker Compose

The Docker Compose is meant for users wishing to try out Phobos and can be run with a simple command. It includes all the components necessary to create pipelines within Phobos.

### Requirements

- [Docker Compose](https://docs.docker.com/compose/)
- [Git](https://git-scm.com/) (only for cloning)

The `docker-compose.yml` defines the following services:

| Service      | Container name | Purpose                                            |
| ------------ | -------------- | -------------------------------------------------- |
| postgres     | phobos-db      | A database for the Phobos API                      |
| kc_postgres  | keycloak-db    | A database for Keycloak IDP                        |
| keycloak     | phobos-idp     | An IDP for authenticating with the Phobos API      |
| minio        | phobos-store   | An object storage (like AWS S3) for the Phobos API |
| minioconsole | phobos-mc      | Configures Minio or Phobos API's object store      |
| api          | phobos-api     | The Phobos API                                     |
| ui           | phobos-ui      | The Phobos UI                                      |

:::warning not for production use!
Phobos Docker Compose should **not** be used in production or be exposed to the internet.
:::

### Usage guide

Either clone the [Phobos API](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api.git) repository or download the contents of the `docker-compose` directory.

```bash title="Cloning the Phobos API repository and using docker-compose" showLineNumbers
git clone https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api.git
cd phobos-api/docker-compose  # directory might be different!
sudo docker compose up -d    # "-d" means start in the background.
sudo docker logs phobos-api # Optionally, check for Phobos API logs.
```

<details>
<summary>Expand for explanation</summary>

Above will begin pulling the latest images from the appropriate sources and start them up in the background. Once all the services have finished booting up, we can visit the UI at `http://localhost:9001`, which should immediately redirect us to Keycloak for authentication. An admin user is already created with credentials `martian`.

:::note
The Phobos API may take some time starting up as it waits for other services to be in a functioning state.
:::

</details>

## Frequently asked questions (FAQ)

### Can I create pipelines with a local Docker Compose instance?

Yes! The Docker Compose includes all the service dependencies for running pipelines and managing states with Phobos. The database will store all the resource information and Minio will store the objects (Pipeline templates).
