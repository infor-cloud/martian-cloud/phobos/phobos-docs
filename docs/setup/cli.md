---
title: CLI
description: "An installation and build guide for the Phobos CLI"
---

<!-- Import for Tabs used below -->

import Tabs from "@theme/Tabs";
import TabItem from "@theme/TabItem";

The [Phobos CLI](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli) supports all major platforms, and binaries are released via [GitLab releases](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/-/releases).

:::tip Have a question?
Check the [FAQ](#frequently-asked-questions-faq) to see if there's already an answer.
:::

## Download binary

- Two options available for downloading:

  1. Directly from the GitLab releases page if access to a web browser is available.

  2. Using the cURL command for CI/CD or headless setup.

### Directly from GitLab Releases

Latest binaries are available to download from [GitLab releases](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/-/releases).

Download the respective binary for your platform and run it using `./phobos`.

### Using [cURL](https://curl.se/) command

<h4>Available Platforms</h4>
<Tabs groupId="operating-systems">
  <TabItem value="win" label="Windows">

    windows_amd64

    windows_386

  </TabItem>
  <TabItem value="mac" label="macOS">

    darwin_arm64

    darwin_amd64

  </TabItem>
  <TabItem value="linux" label="Linux">

    linux_arm64

    linux_arm

    linux_amd64

    linux_386

  </TabItem>
  <TabItem value="solaris" label="Oracle Solaris">

    solaris_amd64

  </TabItem>
  <TabItem value="open" label="OpenBSD">

    openbsd_amd64

    openbsd_386

  </TabItem>
  <TabItem value="free" label="FreeBSD">

    freebsd_arm

    freebsd_amd64

    freebsd_386

  </TabItem>
</Tabs>

To download the desired binary, copy the following and replace the values next to each environment variable. Then, run the command in a terminal window.

```bash title="Copy the command and replace values" showLineNumbers
export PRIVATE_TOKEN='your-personal-access-token'
export PROJECT_NUMBER='your-project-number'
export VERSION='your-version'
export PLATFORM='platform-from-the-list-above'

curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
  https://gitlab.com/api/v4/projects/$PROJECT_NUMBER/packages/generic/phobos-cli/$VERSION/phobos_$VERSION_$PLATFORM \
  --output phobos
```

:::note
The GitLab personal access token will need permission to access the CLI project to download the binary.
:::

:::caution
Installing the Phobos CLI on other platforms may be possible, but it is not recommended nor supported.
:::

## Build from source

While downloading a pre-built binary meets most use cases, it may be desired to build a binary from source. Doing so will allow changing the default Phobos endpoint (`DEFAULT_ENDPOINT_URL`) the CLI uses, which offers some convenience when configuring the CLI.

The CLI project includes a handy `Makefile` to help make the build process a little easier.

### Requirements

- [Go](https://go.dev/doc/install)
- [Make](https://www.gnu.org/software/make/)

```bash title="Git clone the project to the local machine"
git clone https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli.git
```

```bash showLineNumbers title="Use make to compile a binary" showLineNumbers
cd <project-directory>
export DEFAULT_ENDPOINT_URL='https://api.phobos.example.com' # Optional.
make build
```

## Frequently asked questions (FAQ)

### How can I make the binary an executable?

On a Linux system, `cd` into the CLI's directory and run `chmod +x phobos`. Run with `./phobos`

For Windows users, it _may_ be necessary to append `.exe` to the binary name to make it an executable. For example, `--output phobos.exe` in the `cURL` command above or by simply renaming the file after downloading.

### Is there a way to run the CLI from anywhere on my system?

Yes. If on a Linux system, and using [Bash shell](<https://en.wikipedia.org/wiki/Bash_(Unix_shell)>), run the following command in a terminal window:

```bash
echo "PATH=\$PATH:[binary path]" >> ~/.bashrc && source ~/.bashrc
```

> Replace `[binary path]` with the full path to the CLI binary.

Please consult the internet for exporting to `PATH` on different platforms.

### Does the CLI auto-update?

At the moment, it does not. A new binary will have to be manually downloaded using the above methods. In a CI/CD environment, this can be automated.

### Is the CLI binary signed?

Not yet, although this is on our roadmap.
