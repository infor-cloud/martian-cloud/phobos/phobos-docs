---
title: UI
description: "An installation and build guide for the Phobos UI"
---

The UI does not yet have any binaries. However, it is possible to run it locally with the following instructions.

## Build and run locally (Docker)

:::tip
Even easier, use the provided Docker images. Learn [more](docker.md).
:::

### Requirements

- The [Phobos API](api.md)
- [Node](https://nodejs.org/en/download/)

### Build from source

Prior to proceeding, ensure the Phobos API is up and running.

```bash title="Git clone the project to the local machine"
git clone https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-ui.git
```

```bash showLineNumbers title="Build and start the server with npm" showLineNumbers
cd phobos-ui
npm install
npm start
```

- Point web browser to http://localhost:9001/ if not done automatically.

## Environment variables

<details>
<summary>Expand for a complete list and explanation</summary>

|                            Name |     Generic Value     | Description             |
| ------------------------------: | :-------------------: | ----------------------- |
| `REACT_APP_PHOBOS_API_ENDPOINT` | http://localhost:9000 | URL for the Phobos API. |

</details>
