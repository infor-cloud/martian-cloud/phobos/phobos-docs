---
title: Introduction
description: "What is Phobos?"
---

A modern, [open-source](https://gitlab.com/groups/infor-cloud/martian-cloud/phobos), self-hosted release orchestration and management platform. Simplify any release process and automate your deployment pipelines.

Leveraging the powerful [HashiCorp Configuration Language (HCL)](https://github.com/hashicorp/hcl), Phobos eliminates the need for complex, hard-to-maintain scripts and provides a simple, intuitive, and declarative syntax to define any release process. Execute your release process with confidence, knowing that Phobos will provide you with the tools to manage and monitor your deployments. Extend the platform with custom plugins and integrations to fit your unique needs. Phobos is designed to be flexible and extensible, allowing you to integrate with your existing tools and processes.

## Some key features

### Phobos API

&#x2705; Powerful and flexible release process definition using HCL.

&#x2705; Ability to orchestrate release deployments with pipelines.

&#x2705; Machine to Machine (M2M) authentication with service accounts.

&#x2705; Virtually unlimited extensibility with custom plugins.

&#x2705; Upload and manage pipeline and release templates.

&#x2705; Tailor releases to specific environments.

&#x2705; Govern who can trigger deployments via approval rules.

&#x2705; Plugin registry for easy discovery and installation of plugins.

&#x2705; Bring your own agent to execute jobs or use the shared agent pool.

&#x2705; Define tags for your agents to match your job requirements.

&#x2705; Retry failed jobs, releases, deployments, or pipelines.

&#x2705; [Role-based access control (RBAC)](https://en.wikipedia.org/wiki/Role-based_access_control) for users and service accounts.

&#x2705; Support for [GraphQL](https://graphql.org/) and [gRPC](https://grpc.io/) APIs.

&#x2705; Easily develop your own tools with available packages with no additional SDKs.

&#x2705; Written in [Go](https://go.dev/).

### Phobos UI

&#x2705; Approve, schedule, and trigger deployments on demand.

&#x2705; View and manage your release lifecycle and pipeline templates with built-in [Monaco](https://microsoft.github.io/monaco-editor/) editor.

&#x2705; Create, fine-tune, and manage your releases and pipelines.

&#x2705; Monitor agent health and activity to easily troubleshoot issues.

&#x2705; Access control with RBAC for users and service accounts.

&#x2705; Bird's eye view of changes with activity events at the organization or project level.

&#x2705; Use comments to discuss and collaborate on releases and pipelines.

&#x2705; Access to the plugin registry.

&#x2705; Powered by GraphQL subscriptions for live event-driven updates.

&#x2705; Written in [TypeScript](https://www.typescriptlang.org/).

### Phobos CLI

&#x2705; Trigger pipelines straight from your terminal.

&#x2705; Version, upload, and validate pipeline and release templates.

&#x2705; Interface with the plugin registry to discover, install plugins, and more.

&#x2705; Unlimited profiles for use with different Phobos instances.

&#x2705; Powered by gRPC for fast and efficient communication.

&#x2705; CRUD operations on most resources.

&#x2705; Written in [Go](https://go.dev/).

### Phobos Agent

&#x2705; Execute jobs and releases on your infrastructure.

&#x2705; Run jobs in a containerized environment.

&#x2705; Use tags to match your agents to your job requirements.

&#x2705; Diagnose and troubleshoot your agents with built-in sessions and logs.

&#x2705; Assign service accounts for effortless authentication.

&#x2705; Written in [Go](https://go.dev/).
