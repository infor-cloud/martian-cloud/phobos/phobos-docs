import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HomepageFeatures from "@site/src/components/HomepageFeatures";
import styles from "./index.module.css";

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={"hero hero--primary"}>
      <div className="container">
        <h1 className={styles.heroTitle}>{siteConfig.title}</h1>
        <p className={styles.heroSubtitle}>{siteConfig.tagline}</p>
        <p id={styles.description}>
          Scalable, high-performance, and painless release management for any
          project. A modern platform for continuous delivery and deployment
          orchestration. Open-source.
        </p>
      </div>
    </header>
  );
}

export default function Home() {
  return (
    <Layout title={`Documentation`} description="Phobos documentation site">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
