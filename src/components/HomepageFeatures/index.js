import React from "react";
import styles from "./styles.module.css";
import Link from "@docusaurus/Link";
import FeatureGallery from "./feature_gallery";
import DownloadCards from "./download_cards";
import Heading from "@theme/Heading";

export default function HomepageFeatures() {
  return (
    <div>
      <section id={styles.quickStartContainer}>
        <div id={styles.quickStart}>
          <Heading as="h1">New to Phobos?</Heading>
          <p id={styles.quickStartDescription}>
            Learn the ABCs of creating a pipeline with Phobos in just a few
            minutes. We'll show you the ropes and get you started in no time. No
            prior knowledge is required!
          </p>
          <Link id={styles.quickStartButton} to={"quickstart"}>
            Take me there!
          </Link>
        </div>
      </section>
      <FeatureGallery />
      <DownloadCards />
    </div>
  );
}
