import React from "react";
import styles from "./styles.module.css";
import Link from "@docusaurus/Link";
import Heading from "@theme/Heading";

interface DownloadProps {
  title: string;
  url: string;
  description: string;
  buttonLabel: string;
}

function DownloadCard({ title, description, url, buttonLabel }: DownloadProps) {
  return (
    <div id={styles.downloadCard}>
      <h4>{title}</h4>
      {description}
      <footer>
        <Link id={styles.downloadButton} to={url}>
          {buttonLabel}
        </Link>
      </footer>
    </div>
  );
}

function DownloadCards() {
  return (
    <section id={styles.downloadContainer}>
      <Heading as="h1" style={{ marginRight: 30 }}>
        Installation
      </Heading>
      <div>
        <DownloadCard
          title="Docker Compose"
          description="Run Phobos in Docker with just one command."
          buttonLabel="Get Started"
          url="/setup/docker#docker-compose"
        ></DownloadCard>
        <DownloadCard
          title="API Image"
          description="Pre-built API, agent, and job executor Docker containers."
          buttonLabel="Download"
          url="https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/container_registry"
        ></DownloadCard>
        <DownloadCard
          title="UI Image"
          description="Pre-built UI Docker container."
          buttonLabel="Download"
          url="https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-ui/container_registry"
        ></DownloadCard>
        <DownloadCard
          title="CLI Binary"
          description="Ready-to-use CLI binaries supported on all major platforms."
          buttonLabel="Download"
          url="https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/-/releases"
        ></DownloadCard>
      </div>
    </section>
  );
}

export default DownloadCards;
