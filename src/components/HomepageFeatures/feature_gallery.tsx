import React from "react";
import styles from "./styles.module.css";

function FeatureGallery() {
  return (
    <section className={styles.featuresContainer}>
      <div className={styles.featureItem}>
        <div className={styles.featureDescription}>
          <h2>Simple Configurations</h2>
          <p>
            Create maintainable release and pipeline definitions using the
            HashiCorp Configuration Language (HCL). No need for complex scripts
            or custom code. Keep templates clean, error-free, and easy to
            understand. Validate configurations with a single command prior to
            deployment.
          </p>
        </div>
        <img
          src="img/homepage/hcl.png"
          alt="Image of a sample pipeline configuration."
        ></img>
      </div>
      <div className={styles.featureItem}>
        <img
          src="img/homepage/plugin.png"
          alt="Image of an extension icon."
          className={styles.featureIcon}
        ></img>
        <div className={styles.featureDescription}>
          <h2>Endless Extensibility</h2>
          <p>
            Virtually endless possibilities for extending Phobos with custom
            plugins. Create your own plugins to integrate with your existing
            tools and services. Share your plugins with the community using the
            plugin registry. Imagination is the only limit.
          </p>
        </div>
      </div>
      <div className={styles.featureItem}>
        <div className={styles.featureDescription}>
          <h2>Built-in Plugin Registry</h2>
          <p>
            Discover and use plugins from the Phobos plugin registry. Browse
            through a wide range of plugins shared by the community. Upload your
            plugins with a single command and share them with other
            organizations. Optionally, keep your plugins private and use them
            within your organization only.
          </p>
        </div>
        <img
          src="img/homepage/registry.png"
          alt="Image of plugin registry."
          className={styles.featureIcon}
        ></img>
      </div>
      <div className={styles.featureItem}>
        <img
          src="img/homepage/activity.png"
          alt="Image of activity events."
        ></img>
        <div className={styles.featureDescription}>
          <h2>Monitoring and Observability</h2>
          <p>
            Monitor changes to your pipelines and releases in real-time with
            activity events built right into the UI. See who made changes within
            your organization and when, or filter down to a specific project.
          </p>
        </div>
      </div>
      <div className={styles.featureItem}>
        <div className={styles.featureDescription}>
          <h2>Comprehensive CLI</h2>
          <p>
            Built with gRPC, the CLI provides a comprehensive set of commands
            for managing your pipelines. Use the CLI to create, run, and manage
            your pipelines. Automate your pipeline management tasks with the CLI
            and integrate Phobos with your existing CI/CD workflows.
          </p>
        </div>
        <img src="img/homepage/cli.png" alt="Image of the CLI."></img>
      </div>
      <div className={styles.featureItem}>
        <img
          src="img/homepage/service-account.png"
          alt="Image of service accounts."
          className={styles.featureIcon}
        ></img>
        <div className={styles.featureDescription}>
          <h2>Effortless M2M Authentication</h2>
          <p>
            No need to manage API keys or secrets. Authenticate external
            services and tools with Phobos using Service Accounts. Control
            access to your pipelines and releases with RBAC available out of the
            box.
          </p>
        </div>
      </div>
      <div className={styles.featureItem}>
        <div className={styles.featureDescription}>
          <h2>Bring Your Own Agent</h2>
          <p>
            Benefit from the flexibility of running jobs on your own agent.
            Simply assign them to your organization and use your own
            infrastructure to run your jobs and maintain full control over your
            data and resources. Monitor agent health and status right from the
            UI.
          </p>
        </div>
        <img
          src="img/homepage/agent.png"
          alt="Image of agents."
          className={styles.featureIcon}
        ></img>
      </div>
    </section>
  );
}

export default FeatureGallery;
