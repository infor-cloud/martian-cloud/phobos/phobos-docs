// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const { themes } = require("prism-react-renderer");
const lightCodeTheme = themes.github;
const darkCodeTheme = themes.dracula;
const defaultURI = "http://localhost:3000";

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Phobos",
  titleDelimiter: "🔄",
  tagline: "Hassle-free release management and deployment orchestration.",
  url: String(process.env.DOCS_URL || defaultURI),
  baseUrl: String(process.env.DOCS_BASE_URL || "/"),
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",

  // TODO: Replace this with Phobos logo when that happens.
  favicon: "img/favicon.ico",

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  plugins: [
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} **/
      {
        indexBlog: false,
        docsRouteBasePath: "/",
        hashed: true,
      },
    ],
    [
      "docusaurus-plugin-remote-content",
      {
        name: "phobos-cli-generated-markdown",
        sourceBaseUrl:
          "https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/-/raw/main/docs/",
        outDir: "docs/cli",
        documents: ["commands.md"],
      },
    ],
  ],

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: "/",
          sidebarPath: require.resolve("./sidebars.js"),
        },
        blog: false,
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "Phobos",
        logo: {
          alt: "Phobos Logo",

          // TODO: Replace this with Phobos logo when that happens.
          src: "img/favicon.ico",
        },
        hideOnScroll: true,
        items: [
          {
            to: "intro",
            position: "left",
            label: "Docs",
          },
          {
            href: "https://gitlab.com/groups/infor-cloud/martian-cloud/phobos",
            label: "It's open source",
            position: "right",
          },
        ],
      },
      colorMode: {
        respectPrefersColorScheme: true,
      },
      docs: {
        sidebar: {
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      footer: {
        style: "dark",
        logo: {
          alt: "Infor Logo",
          src: "img/favicon.ico",
          href: "https://www.infor.com/",
          target: "_blank",
          style: { marginTop: 0 },
          width: 100,
          height: 100,
        },
        links: [
          {
            title: "Get Started",
            items: [
              {
                label: "Introduction",
                to: "intro",
              },
              {
                label: "Quickstart",
                to: "quickstart",
              },
            ],
          },
          {
            title: "Guides",
            items: [
              {
                label: "Agents",
                to: "guides/agents",
              },
              {
                label: "Memberships",
                to: "guides/memberships",
              },
              {
                label: "Service Accounts",
                to: "guides/service_accounts",
              },
            ],
          },
          {
            title: "Plugins",
            items: [
              {
                label: "Exec",
                to: "plugins/exec",
              },
            ],
          },
          {
            title: "Feature Requests",
            items: [
              {
                label: "GitLab",
                href: "https://gitlab.com/groups/infor-cloud/martian-cloud/phobos",
              },
            ],
          },
          {
            title: "Follow Us",
            items: [
              {
                label: "LinkedIn",
                href: "https://www.linkedin.com/company/infor",
              },
              {
                label: "Twitter",
                href: "https://twitter.com/infor",
              },
              {
                label: "Facebook",
                href: "https://facebook.com/infor",
              },
              {
                label: "YouTube",
                href: "https://www.youtube.com/c/InforGlobal",
              },
              {
                label: "Instagram",
                href: "https://instagram.com/infor",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Infor. All rights reserved. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,

        // Add any additional languages go here for syntax highlighting.
        additionalLanguages: ["hcl", "graphql", "bash"],
      },
    }),
};

module.exports = config;
