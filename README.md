# Phobos Docs

Phobos Docs is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

This project requires [Git LFS](https://git-lfs.github.com/) for all `.png` and `.ico` files.

## Formatting

Use the [Prettier](https://prettier.io/) extension for proper markdown formatting.

## Local development

1. **Clone the repository and then `cd` into the directory**:

   ```bash
   cd phobos-docs
   ```

2. **Install the dependencies**:

   ```bash
   npm install
   ```

3. **Start the development server**:

   ```bash
    npm start
   ```

## License

Phobos Docs is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
