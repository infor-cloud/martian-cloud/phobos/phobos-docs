/**
Any new items added to the docs will need to be updated
in this sidebar as well or else they won't show up.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
    docs: [
        "intro",
        "quickstart",
        {
            type: "category",
            label: "Guides",
            link: {
                type: "generated-index",
                description: "Guides for Phobos.",
                keywords: ["guides", "guide", "how-to", "howto", "tutorials"],
                slug: "guides",
            },
            items: ["guides/organizations", "guides/projects", "guides/releases",
                {
                    type: "category",
                    label: "Release Lifecycles",
                    items: [
                        "guides/release_lifecycles", "guides/release_lifecycle_template_syntax"
                    ],
                }, "guides/pipelines",
                {
                    type: "category",
                    label: "Pipeline Templates",
                    items: [
                        "guides/pipeline_templates", "guides/pipeline_template_syntax"
                    ],
                }, "guides/variables", "guides/environments", "guides/approval_rules", "guides/vcs_providers", "guides/service_accounts", "guides/agents", "guides/memberships"
            ],
        },
        {
            type: "category",
            label: "CLI",
            link: {
                type: "generated-index",
                description: "All about Phobos CLI.",
                keywords: [
                    "cli",
                    "command",
                    "command-line",
                    "interface",
                    "terminal",
                    "shell",
                ],
                slug: "cli",
            },
            items: ["cli/intro", "cli/commands"],
        },
        {
            type: "category",
            label: "Setup",
            link: {
                type: "generated-index",
                description: "Setup guides for Phobos API, CLI and UI.",
                keywords: [
                    "setup",
                    "install",
                    "installation",
                    "configure",
                    "configuration",
                ],
                slug: "setup",
            },
            items: ["setup/api", "setup/ui", "setup/cli", "setup/docker"],
        },
        {
            type: "category",
            label: "Plugins",
            link: {
                type: "generated-index",
                description: "All about Phobos plugins.",
                keywords: [
                    "plugins",
                    "plugin",
                    "extend",
                    "extension",
                    "exec",
                    "execute",
                    "shell"
                ],
                slug: "plugins",
            },
            items: [
                "plugins/intro",
                {
                    type: "category",
                    label: "Built-in",
                    link: {
                        type: "doc",
                        id: "plugins/built-in",
                    },
                    items: [
                        "plugins/exec"
                    ],
                },
            ],
        },
    ],
};

module.exports = sidebars;
